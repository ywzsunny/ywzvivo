import torch



def _tensor_joint(temp_feat):
    # temp_feat = [p2,p3,p4,p5]
    out_channels = (int)((1+1/4+1/16+1/64)*temp_feat[0].shape[1]) #长宽都是2倍2倍降
    temp_tensor = torch.zeros(temp_feat[0].shape[0], padding_size(out_channels, 3), padding_size(temp_feat[0].shape[2], 128), padding_size(temp_feat[0].shape[3], 128))
    channel_start = 0
    for ii in range(len(temp_feat)):
        size_num = 2**ii
        delta_channel_num = temp_feat[0].shape[1] // (size_num * size_num)
        channel_end = channel_start + delta_channel_num
        little_row = temp_feat[ii].shape[2]
        little_col = temp_feat[ii].shape[3]
        channel_idx = 0
        for row in range(size_num):
            for col in range(size_num):
                temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)] = \
                    temp_feat[ii][:, channel_idx:(channel_idx + delta_channel_num),:,:]
                channel_idx += delta_channel_num
        channel_start = channel_end
    return temp_tensor

def _tensor_split(temp_tensor, ori_feat0_shape):
    temp_feat = []
    len_temp_feat = 0
    temp_channels = temp_tensor.shape[1]
    cnt_channels = ori_feat0_shape[1]
    # print("ori channels:{}".format(cnt_channels))
    while temp_channels >= padding_size(cnt_channels, 3):
        len_temp_feat += 1
        if (temp_channels == padding_size(cnt_channels, 3)):
            break
        cnt_channels += ori_feat0_shape[1] * (1/(4**len_temp_feat))
    ##
    # print("len feat:{}".format(len_temp_feat))
    channel_start = 0
    for ii in range(len_temp_feat):
        size_num = 2**ii
        little_row = ori_feat0_shape[2] // size_num
        little_col = ori_feat0_shape[3] // size_num
        temp_rev_tensor = torch.zeros(ori_feat0_shape[0], ori_feat0_shape[1], little_row, little_col)
        delta_channel_num = ori_feat0_shape[1] // (size_num * size_num)
        channel_end = channel_start + delta_channel_num
        channel_idx = 0
        ##
        for row in range(size_num):
            for col in range(size_num):
                temp_rev_tensor[:, channel_idx:(channel_idx + delta_channel_num),:,:] = \
                    temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)]
                channel_idx += delta_channel_num
        channel_start = channel_end
        ##
        temp_feat.append(temp_rev_tensor)
    return temp_feat

def padding_size(ori_size, factor_size):
    if ori_size % factor_size == 0:
        return ori_size
    else:
        return factor_size * (ori_size // factor_size + 1)


if __name__ == "__main__":
    a = torch.randn([1, 256, 192, 336])
    b = torch.randn([1, 256, 192 // 2, 336 // 2])
    c = torch.randn([1, 256, 192 // 2 // 2, 336 // 2 // 2])
    d = torch.randn([1, 256, 192 // 2 // 2 // 2, 336 // 2 // 2 // 2])
    
    ori_feat_list = [a, b, c, d]
    merged_tensor = _tensor_joint(ori_feat_list)
    print(merged_tensor.shape)
    
    recover_feat_list = _tensor_split(merged_tensor, a.shape)
    for ii in range(len(recover_feat_list)):
        print(recover_feat_list[ii].shape)
        if not recover_feat_list[ii].equal(ori_feat_list[ii]):
            raise ValueError("do not equal")

    print("done")