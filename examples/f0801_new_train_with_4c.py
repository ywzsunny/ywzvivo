# python3 examples/train.py -m cheng2020-attn -d /media/data/yangwenzhe/Dataset/DIV2K/DIV2K_train_HR/ -d_test /media/data/yangwenzhe/Dataset/div_after_crop/ -q 4 --lambda 0.001 --batch-size 6 -lr 1e-5 --save --cuda --exp exp_cheng_En_01_only_q4 --checkpoint /home/jjp/CompressAI/pretrained_model/cheng2020-attn/cheng2020_attn-ms-ssim-4-8b2f647e.pth.tar
import enum
import os
import argparse
import math
import random
from re import M
import shutil
import sys
# sys.path.append(".")
# sys.path.append("..")
import torch
import torch.nn as nn
import torch.optim as optim
import torchvision
import torch.nn.functional as F

from torch.utils.data import DataLoader
from torchvision import transforms
from collections import OrderedDict
from compressai.models import *

import logging
# from utils import util
import os
import logging
from datetime import datetime

def get_timestamp():
    return datetime.now().strftime('%y%m%d-%H%M%S')

def setup_logger(logger_name, root, phase, level=logging.INFO, screen=False, tofile=False):
    '''set up logger'''
    lg = logging.getLogger(logger_name)
    formatter = logging.Formatter('%(asctime)s.%(msecs)03d - %(levelname)s: %(message)s',
                                  datefmt='%y-%m-%d %H:%M:%S')
    lg.setLevel(level)
    if tofile:
        log_file = os.path.join(root, phase + '_{}.log'.format(get_timestamp()))
        fh = logging.FileHandler(log_file, mode='w')
        fh.setFormatter(formatter)
        lg.addHandler(fh)
    if screen:
        sh = logging.StreamHandler()
        sh.setFormatter(formatter)
        lg.addHandler(sh)
from compressai.datasets import ImageFolder, FeatureFolder
from compressai.zoo import image_models
from compressai.models.Inv_arch import InvRescaleNet
from compressai.models.Subnet_constructor import subnet
from compressai.models.Enhance import EnModule
from compressai.models.ARCNN import ARCNNModel
from torchvision.transforms import ToPILImage
import numpy as np
import PIL
import PIL.Image as Image
from torchvision.transforms import ToPILImage
from pytorch_msssim import ms_ssim
from typing import Tuple, Union
from torch.utils.tensorboard import SummaryWriter


def torch2img(x: torch.Tensor) -> Image.Image:
    return ToPILImage()(x.cpu().clamp_(0, 1).squeeze())
def mse2psnr(mse):
    # 根据Hyper论文中的内容，将MSE->psnr(db)
    # return 10*math.log10(255*255/mse)
    return 10 * math.log10(1/ mse)

def compute_metrics(
        a: Union[np.array, Image.Image],
        b: Union[np.array, Image.Image],
        max_val: float = 255.0,
) -> Tuple[float, float]:
    """Returns PSNR and MS-SSIM between images `a` and `b`. """
    if isinstance(a, Image.Image):
        a = np.asarray(a)
    if isinstance(b, Image.Image):
        b = np.asarray(b)

    a = torch.from_numpy(a.copy()).float().unsqueeze(0)
    if a.size(3) == 3:
        a = a.permute(0, 3, 1, 2)
    b = torch.from_numpy(b.copy()).float().unsqueeze(0)
    if b.size(3) == 3:
        b = b.permute(0, 3, 1, 2)

    mse = torch.mean((a - b) ** 2).item()
    p = 20 * np.log10(max_val) - 10 * np.log10(mse)
    m = ms_ssim(a, b, data_range=max_val).item()
    return p, m


class RateDistortionLoss(nn.Module):
    """Custom rate distortion loss with a Lagrangian parameter."""

    def __init__(self, lmbda=1e-2, mode = ''):
        super().__init__()
        self.mse = nn.MSELoss()
        self.lmbda = lmbda
        self.mode = mode

    def forward(self, output=None, target=None, lq=None, x_l=None, x_enh=None):
        N, _, H, W = target.size()
        out = {}
        num_pixels = N * H * W

        if not output:
            out["bpp_loss"] = 0
        else:
            out["bpp_loss"] = sum(
                (torch.log(likelihoods).sum() / (-math.log(2) * num_pixels))
                for likelihoods in output["likelihoods"].values()
            )

        if self.mode=='only_codec':
            out["mse_loss"] = self.mse(x_l, x_enh)
            out["loss"] = self.lmbda * 255 ** 2 * out["mse_loss"] + out["bpp_loss"]
        elif self.mode=='only_irn':
            out["mse_loss"] = self.mse(lq, target) #大图
            little_gt = F.interpolate(target, scale_factor=0.5, mode='bicubic') ## bicubic
            out["mse_little_loss"] = self.mse(x_l, little_gt)
            out["loss"] = out["mse_little_loss"] + 10*out["mse_loss"]
        else:
            out["mse_loss"] = self.mse(lq, target) #大图
            out["loss"] = self.lmbda * 255 ** 2 * out["mse_loss"] + out["bpp_loss"]
        # out["loss"] = self.mse(x_l, x_enh)
        # out["loss"] = self.mse(x_l, x_enh) + out["mse_loss"]
        return out


class AverageMeter:
    """Compute running average."""

    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class CustomDataParallel(nn.DataParallel):
    """Custom DataParallel to access the module methods."""

    def __getattr__(self, key):
        try:
            return super().__getattr__(key)
        except AttributeError:
            return getattr(self.module, key)


def configure_optimizers(net, args):
    """Separate parameters for the main optimizer and the auxiliary optimizer.
    Return two optimizers"""

    parameters = {
        n
        for n, p in net.named_parameters()
        if not n.endswith(".quantiles") and p.requires_grad
    }
    aux_parameters = {
        n
        for n, p in net.named_parameters()
        if n.endswith(".quantiles") and p.requires_grad
    }

    # Make sure we don't have an intersection of parameters
    params_dict = dict(net.named_parameters())
    inter_params = parameters & aux_parameters
    union_params = parameters | aux_parameters

    assert len(inter_params) == 0
    assert len(union_params) - len(params_dict.keys()) == 0

    optimizer = optim.Adam(
        (params_dict[n] for n in sorted(parameters)),
        lr=args.learning_rate,
    )
    aux_optimizer = optim.Adam(
        (params_dict[n] for n in sorted(aux_parameters)),
        lr=args.aux_learning_rate,
    )
    return optimizer, aux_optimizer


def Net_optimizers(net, learning_rate):
    parameters = {
        n for n, p in net.named_parameters() if p.requires_grad
    }

    params_dict = dict(net.named_parameters())

    optimizer = optim.Adam(
        (params_dict[n] for n in sorted(parameters)),
        lr=learning_rate,
    )

    return optimizer

def padding_size(ori_size, factor_size):
    if ori_size % factor_size == 0:
        return ori_size
    else:
        return factor_size * (ori_size // factor_size + 1)


def _tensor_joint(temp_feat, channel_factor = 3):
    # temp_feat = [p2,p3,p4,p5]
    out_channels = (int)((1+1/4+1/16+1/64)*temp_feat[0].shape[1]) #长宽都是2倍2倍降
    temp_tensor = torch.zeros(temp_feat[0].shape[0], padding_size(out_channels, channel_factor), padding_size(temp_feat[0].shape[2], 128), padding_size(temp_feat[0].shape[3], 128))
    channel_start = 0
    for ii in range(len(temp_feat)):
        size_num = 2**ii
        delta_channel_num = temp_feat[0].shape[1] // (size_num * size_num)
        channel_end = channel_start + delta_channel_num
        little_row = temp_feat[ii].shape[2]
        little_col = temp_feat[ii].shape[3]
        channel_idx = 0
        for row in range(size_num):
            for col in range(size_num):
                temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)] = \
                    temp_feat[ii][:, channel_idx:(channel_idx + delta_channel_num),:,:]
                channel_idx += delta_channel_num
        channel_start = channel_end
    return temp_tensor

def _tensor_split(temp_tensor, ori_feat0_shape, channel_factor = 3):
    temp_feat = []
    len_temp_feat = 0
    temp_channels = temp_tensor.shape[1]
    cnt_channels = ori_feat0_shape[1]
    # print("ori channels:{}".format(cnt_channels))
    while temp_channels >= padding_size(cnt_channels, channel_factor):
        len_temp_feat += 1
        if (temp_channels == padding_size(cnt_channels, channel_factor)):
            break
        cnt_channels += ori_feat0_shape[1] * (1/(4**len_temp_feat))
    ##
    # print("len feat:{}".format(len_temp_feat))
    channel_start = 0
    for ii in range(len_temp_feat):
        size_num = 2**ii
        little_row = ori_feat0_shape[2] // size_num
        little_col = ori_feat0_shape[3] // size_num
        temp_rev_tensor = torch.zeros(ori_feat0_shape[0], ori_feat0_shape[1], little_row, little_col)
        delta_channel_num = ori_feat0_shape[1] // (size_num * size_num)
        channel_end = channel_start + delta_channel_num
        channel_idx = 0
        ##
        for row in range(size_num):
            for col in range(size_num):
                temp_rev_tensor[:, channel_idx:(channel_idx + delta_channel_num),:,:] = \
                    temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)]
                channel_idx += delta_channel_num
        channel_start = channel_end
        ##
        temp_feat.append(temp_rev_tensor)
    return temp_feat


def train_one_epoch(
        model, IRN_net, En_net, criterion, train_dataloader, optimizer, optimizer_IRN, optimizer_En, aux_optimizer,
        epoch, clip_max_norm, logger_train, tb_logger
):
    model.train()
    IRN_net.train()
    # En_net.train()
    device = next(model.parameters()).device
    # for i, d in enumerate(train_dataloader):
    for i,temp_data in enumerate(train_dataloader):
        optimizer.zero_grad()
        aux_optimizer.zero_grad()

        optimizer_IRN.zero_grad()
        d_ori, temp_path = temp_data[0], temp_data[1]
        # raise ValueError("d_ori shape:{}".format(d_ori.shape))
        # p2,p3,p4,p5, temp_path, jpg_size = temp_data[0], temp_data[1], temp_data[2], temp_data[3], temp_data[4], temp_data[5]
        # jpg_pixels = jpg_size[0] * jpg_size[1]
        # temp_feat = [p2, p3, p4, p5]
        # temp_feat = [p2]
        # for temp_feat_idx in range(len(temp_feat)):
        #
        # d_ori = _tensor_joint(temp_feat, channel_factor=4)
        # d_ori = (d_ori + 12.5)/25
        d_ori = (d_ori + 23.1728) / 43.6082
        temp_ori_size = d_ori.shape
        target_size = [d_ori.size()[0], d_ori.size()[1], padding_size(d_ori.size()[2], 128), padding_size(d_ori.size()[3], 128)]
        # raise ValueError("{}->{}".format(temp_ori_size, target_size))
        d_big = torch.zeros(target_size)
        d_big[:,0:temp_ori_size[1], 0:temp_ori_size[2], 0:temp_ori_size[3]] = d_ori
        d_out_merge = torch.zeros(temp_ori_size)
        if global_mode == 'only_irn':
            for channel_idx in range(d_big.size()[1]//4): # 342
                optimizer_IRN.zero_grad()
                d = d_big[:, channel_idx*4 : (channel_idx*4+4) ,:,:]
                d = d.to(device)
                # interface
                x_l = IRN_inference(IRN_net, d, device)
            #
                x_hat = IRN_inference(IRN_net, x_l, device, rev=True)
                out_criterion = criterion(None, d, x_hat, x_l, None)
                ##
                out_criterion["loss"].backward()
                if clip_max_norm > 0:
                    # torch.nn.utils.clip_grad_norm_(model.parameters(), clip_max_norm)
                    torch.nn.utils.clip_grad_norm_(IRN_net.parameters(), clip_max_norm)
                    # torch.nn.utils.clip_grad_norm_(En_net.parameters(), clip_max_norm)
                optimizer_IRN.step()
                d_out_merge[:,channel_idx*4: (channel_idx*4+4),:,:] = x_hat[:, :,0:temp_ori_size[2],0:temp_ori_size[3]]
                ####
            # merged
            # TODO d_ori = (d_ori + 23.1728) / 43.6082
            aux_loss = model.aux_loss()
            # end channels
        elif global_mode == 'codec_with_irn':
            IRN_net.eval()
            down_sample_tensor_input = torch.zeros([d_ori.size()[0], d_ori.size()[1], padding_size(d_ori.size()[2], 128) // 2, padding_size(d_ori.size()[3], 128) // 2]).to(device)
            down_sample_tensor_output = torch.zeros_like(down_sample_tensor_input).to(device)
            for channel_idx in range(d_big.size()[1]//4): # 342
                d = d_big[:, channel_idx*4 : (channel_idx*4+4) ,:,:]
                d = d.to(device)
                # interface
                x_l = IRN_inference(IRN_net, d, device)
                down_sample_tensor_input[:,channel_idx*4: (channel_idx*4+4),:,:] = x_l[:, :, :, :]
            #codec
            # out_net = model(down_sample_tensor_input)
            # down_sample_tensor_output = out_net["x_hat"]
            # for channel_idx in range(d_big.size()[1]//4): # 342

                ###
                # debug version: only 4c
                out_net = model(x_l)
                down_sample_tensor_output[:,channel_idx*4: (channel_idx*4+4),:,:] = out_net["x_hat"]
                ###
                down_sample_tensor_cpatch = down_sample_tensor_output[:, channel_idx*4:(channel_idx+1)*4, :,:]
                down_sample_tensor_cpatch.to(device)
                x_hat = IRN_inference(IRN_net, down_sample_tensor_cpatch, device, rev=True)
                d_out_merge[:,channel_idx*4: (channel_idx*4+4),:,:] = x_hat[:, :,0:temp_ori_size[2],0:temp_ori_size[3]]
            # merged
            out_criterion = criterion(out_net, d_big, d_out_merge, None, None)
            
            ##
            out_criterion["loss"].backward()
            if clip_max_norm > 0:
                torch.nn.utils.clip_grad_norm_(model.parameters(), clip_max_norm)
                # torch.nn.utils.clip_grad_norm_(IRN_net.parameters(), clip_max_norm)
                # torch.nn.utils.clip_grad_norm_(En_net.parameters(), clip_max_norm)
            aux_loss = model.aux_loss()
            optimizer.step()
            aux_loss.backward()
            aux_optimizer.step()
        # TODO d_ori = (d_ori + 23.1728) / 43.6082
        # end all features
        #########################
        if i % 10 == 0:
            logger_train.info(
                f"Train epoch {epoch}: ["
                f"{i * len(d):5d}/{len(train_dataloader.dataset)}"
                f" ({100. * i / len(train_dataloader):.0f}%)] "
                f'Loss: {out_criterion["loss"]} | ' #.item():.4f
                f'Bpp loss: {out_criterion["bpp_loss"]} | '
                f"Aux loss: {aux_loss}"
            )
            tb_logger.add_scalar('{}'.format('[train]: loss'), out_criterion["loss"], i)
            tb_logger.add_scalar('{}'.format('[train]: bpp_loss'), out_criterion["bpp_loss"], i)
            tb_logger.add_scalar('{}'.format('[train]: mse_loss'), out_criterion["mse_loss"], i)
            tb_logger.add_scalar('{}'.format('[train]: mse_loss'), out_criterion["mse_loss"], i)
            tb_logger.add_image('CODEC_little_GT', down_sample_tensor_input[0, 0:1, :, :], global_step=i, dataformats='CHW')  # dataformats='HWC')
            tb_logger.add_image('CODEC_little_LQ', down_sample_tensor_output[0, 0:1, :, :], global_step=i, dataformats='CHW')
            tb_logger.add_image('IRN_IN', d_big[0, 0:1, :, :], global_step=i, dataformats='CHW')  # dataformats='HWC')
            tb_logger.add_image('RIRN_OUT', d_out_merge[0, 0:1, :, :], global_step=i, dataformats='CHW')
    # tb_logger.add_scalar('{}'.format('[train]: loss'), out_criterion["loss"], epoch)
    # tb_logger.add_scalar('{}'.format('[train]: bpp_loss'), out_criterion["bpp_loss"], epoch)
    # tb_logger.add_scalar('{}'.format('[train]: mse_loss'), out_criterion["mse_loss"], epoch)


def test_epoch(epoch, test_dataloader, model, IRN_net, En_net, criterion, logger_val, tb_logger):
    model.eval()
    device = next(model.parameters()).device

    loss = AverageMeter()
    bpp_loss = AverageMeter()
    mse_loss = AverageMeter()
    aux_loss = AverageMeter()
    psnr = AverageMeter()
    ms_ssim = AverageMeter()

    psnr_1 = AverageMeter()
    psnr_2 = AverageMeter()

    with torch.no_grad():
        for temp_data in test_dataloader:
            p2,p3,p4,p5, temp_path, jpg_size = temp_data[0], temp_data[1], temp_data[2], temp_data[3], temp_data[4], temp_data[5]
            temp_feat = [p2, p3, p4, p5]
            jpg_pixels = jpg_size[0] * jpg_size[1]
            d_ori = _tensor_joint(temp_feat, channel_factor=4)
            d_ori = (d_ori + 23.1728) / 43.6082
            # ywz add 
            # temp_ori_size = d_ori.shape
            # target_size = [d_ori.size()[0], padding_size(d_ori.size()[1], 3), padding_size(d_ori.size()[2], 128), padding_size(d_ori.size()[3], 128)]
            temp_ori_size = d_ori.shape
            target_size = [d_ori.size()[0], d_ori.size()[1], padding_size(d_ori.size()[2], 128), padding_size(d_ori.size()[3], 128)]
            d_big = torch.zeros(target_size)
            d_big[:,0:temp_ori_size[1], 0:temp_ori_size[2], 0:temp_ori_size[3]] = d_ori
            d_out_merge = torch.zeros(temp_ori_size)
            if global_mode == 'only_irn':
                for channel_idx in range(d_big.size()[1]//4): # 342
                    d = d_big[:, channel_idx*4 : (channel_idx*4+4) ,:,:]
                    d = d.to(device)
                    # interface
                    x_l = IRN_inference(IRN_net, d, device)
                    x_hat = IRN_inference(IRN_net, x_l, device, rev=True)
                    out_criterion = criterion(None, d, x_hat, x_l, None)
                    d_out_merge[:,channel_idx*4: (channel_idx*4+4),:,:] = x_hat[:, :,0:temp_ori_size[2],0:temp_ori_size[3]]
                    #
                    loss_p1 = mse2psnr(out_criterion['mse_loss'])
                    loss_p2 = mse2psnr(out_criterion['mse_little_loss'])
                    mse_loss.update(out_criterion["mse_loss"])
                    psnr_1.update(loss_p1)
                    psnr_2.update(loss_p2)
                    ####
                aux_loss = model.aux_loss()
                # end channels
                print("temp_data:mse1:{},psnr1:{},psnr2:{}".format(out_criterion['mse_loss'],loss_p1, loss_p2))

            elif global_mode == 'codec_with_irn':
                down_sample_tensor_input = torch.zeros([d_ori.size()[0], d_ori.size()[1], padding_size(d_ori.size()[2], 128) // 2, padding_size(d_ori.size()[3], 128) // 2]).to(device)
                down_sample_tensor_output = torch.zeros_like(down_sample_tensor_input).to(device)
                for channel_idx in range(d_big.size()[1]//4): # 342
                    d = d_big[:, channel_idx*4 : (channel_idx*4+4) ,:,:]
                    d = d.to(device)
                    # interface
                    x_l = IRN_inference(IRN_net, d, device)
                    down_sample_tensor_input[:,channel_idx*4: (channel_idx*4+4),:,:] = x_l[:, :, :, :]
                #codec
                # out_net = model(down_sample_tensor_input)
                # down_sample_tensor_output = out_net["x_hat"]
                # for channel_idx in range(d_big.size()[1]//4): # 342
                    ###
                    # debug version: only 4c
                    out_net = model(x_l)
                    down_sample_tensor_output[:,channel_idx*4: (channel_idx*4+4),:,:] = out_net["x_hat"]
                    ###

                    down_sample_tensor_cpatch = down_sample_tensor_output[:, channel_idx*4:(channel_idx+1)*4, :,:]
                    down_sample_tensor_cpatch.to(device)
                    x_hat = IRN_inference(IRN_net, down_sample_tensor_cpatch, device, rev=True)
                    d_out_merge[:,channel_idx*4: (channel_idx*4+4),:,:] = x_hat[:, :,0:temp_ori_size[2],0:temp_ori_size[3]]
                # merged
                out_criterion = criterion(out_net, d_big, d_out_merge, None, None)
                mse_loss.update(out_criterion["mse_loss"])
                bpp_loss.update(out_criterion["bpp_loss"])
                loss_p1 = mse2psnr(out_criterion['mse_loss'])
                psnr_1.update(loss_p1)
                print("temp_data:mse1:{},psnr1:{}, bpp:{}".format(out_criterion['mse_loss'],loss_p1, out_criterion['bpp_loss']))
            # TODO d_ori = (d_ori + 23.1728) / 43.6082
    tb_logger.add_scalar('{}'.format('[val]: loss'), loss.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: bpp_loss'), bpp_loss.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: psnr'), psnr.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: dpsnr'), psnr_2.avg - psnr_1.avg, epoch + 1)
    logger_val.info(
        f"Test epoch {epoch}: Average losses: "
        f"Loss: {loss.avg:.4f} | "
        f"MSE loss: {mse_loss.avg:.4f} | "
        f"Bpp loss: {bpp_loss.avg:.4f} | "
        f"Aux loss: {aux_loss.avg:.2f} | "
        f"PSNR: {psnr.avg:.6f} | "
        f"MS-SSIM: {ms_ssim.avg:.6f} | "
        f"PSNR_1: {psnr_1.avg:.6f} | "
        f"PSNR_2: {psnr_2.avg:.6f} | "
    )
    return loss.avg

def IRN_inference(model, input, device, rev=False):
    if rev:
        b, c, h, w = input.shape
        z = torch.randn([b, 3 * c, h, w]).to(device)
        out = model(torch.cat((input, z), 1), rev)

    else:
        x_forward = model(input)
        out = x_forward[:, :input.shape[1], :, :]

    return out


def save_checkpoint(state, is_best, suffix, filename="checkpoint.pth.tar"):
    torch.save(state, filename)
    if is_best:
        dest_filename = filename.replace(filename.split('/')[-1], suffix + "_checkpoint_best_loss.pth.tar")
        shutil.copyfile(filename, dest_filename)


def load_ARCNN(load_path, network, strict=True):
    if isinstance(network, nn.DataParallel):
        network = network.module
    load_net = torch.load(load_path)
    load_net_clean = OrderedDict()  # remove unnecessary 'module.'
    for k, v in load_net['network']['net'].items():
        if k.startswith('module.'):
            load_net_clean[k[7:]] = v
        else:
            load_net_clean[k] = v
    network.load_state_dict(load_net_clean, strict=strict)


def load_IRN(path, network):
    load_path = path
    load_network(load_path, network, True)


def load_network(load_path, network, strict=True):
    if isinstance(network, nn.DataParallel):
        network = network.module
    load_net = torch.load(load_path)

    load_net_clean = OrderedDict()  # remove unnecessary 'module.'
    for k, v in load_net.items():
        if k.startswith('module.'):
            load_net_clean[k[7:]] = v
        else:
            load_net_clean[k] = v
    network.load_state_dict(load_net_clean, strict=strict)


def load_En(checkpoint, net):
    state_dicts = torch.load(checkpoint)
    network_state_dict = {k: v for k, v in state_dicts['net'].items() if 'tmp_var' not in k}
    net.load_state_dict(network_state_dict)


def parse_args(argv):
    parser = argparse.ArgumentParser(description="Example training script.")
    parser.add_argument(
        "-m",
        "--model",
        default="bmshj2018-factorized",
        choices=image_models.keys(),
        help="Model architecture (default: %(default)s)",
    )
    parser.add_argument(
        "-d", "--dataset", type=str, required=True, help="Training dataset"
    )
    parser.add_argument(
        "-d_test", "--test_dataset", type=str, required=True, help="Training dataset"
    )
    parser.add_argument(
        "-e",
        "--epochs",
        default=5000,
        type=int,
        help="Number of epochs (default: %(default)s)",
    )
    parser.add_argument(
        "-lr",
        "--learning-rate",
        default=1e-4,
        type=float,
        help="Learning rate (default: %(default)s)",
    )
    parser.add_argument(
        "-n",
        "--num-workers",
        type=int,
        default=4,
        help="Dataloaders threads (default: %(default)s)",
    )
    parser.add_argument(
        "-q",
        "--quality",
        type=int,
        default=1,
    )
    parser.add_argument(
        "--lambda",
        dest="lmbda",
        type=float,
        default=1e-2,
        help="Bit-rate distortion parameter (default: %(default)s)",
    )
    parser.add_argument(
        "--rate",
        type=float,
        default=1.0,
        help="train data rate 0-1",
    )
    parser.add_argument(
        "--batch-size", type=int, default=16, help="Batch size (default: %(default)s)"
    )
    parser.add_argument(
        "--test-batch-size",
        type=int,
        default=1,
        help="Test batch size (default: %(default)s)",
    )
    parser.add_argument(
        "--aux-learning-rate",
        default=1e-3,
        help="Auxiliary loss learning rate (default: %(default)s)",
    )
    parser.add_argument(
        "--patch-size",
        type=int,
        nargs=2,
        default=(256, 256),
        help="Size of the patches to be cropped (default: %(default)s)",
    )
    parser.add_argument("--mode", default="", type=str, help="Loss function mode")
    parser.add_argument("--froze_irn", action="store_true", help="froze irn")
    parser.add_argument("--froze_codec", action="store_true", help="froze codec")
    parser.add_argument("--onlytest", action="store_true", help="only_test")
    # parser.add_argument("--cuda", action="store_true", help="Use cuda")
    parser.add_argument(
        "--cuda",
        type=int,
        default=-1,
        help="Use cuda",
    )
    parser.add_argument(
        "--save", action="store_true", default=True, help="Save model to disk"
    )
    parser.add_argument(
        "--seed", type=float, help="Set random seed for reproducibility"
    )
    parser.add_argument(
        "--clip_max_norm",
        default=1.0,
        type=float,
        help="gradient clipping max norm (default: %(default)s",
    )
    parser.add_argument(
        "-exp", "--experiment", type=str, required=True, help="Experiment name"
    )
    parser.add_argument("--irncheckpoint", type=str, help="Path to a irncheckpoint")
    parser.add_argument("--checkpoint", type=str, help="Path to a checkpoint")
    args = parser.parse_args(argv)
    return args


def main(argv):
    args = parse_args(argv)
    global froze_irn, froze_codec, global_mode
    froze_irn = args.froze_irn
    froze_codec = args.froze_codec
    global_mode = args.mode or ''

    if args.seed is not None:
        torch.manual_seed(args.seed)
        random.seed(args.seed)

    if not os.path.exists(os.path.join('experiments', args.experiment)):
        os.makedirs(os.path.join('experiments', args.experiment))

    # util.setup_logger
    setup_logger('train', os.path.join('experiments', args.experiment), 'train_' + args.experiment,
                      level=logging.INFO,
                      screen=True, tofile=True)
    setup_logger('val', os.path.join('experiments', args.experiment), 'val_' + args.experiment,
                      level=logging.INFO,
                      screen=True, tofile=True)

    logger_train = logging.getLogger('train')
    logger_val = logging.getLogger('val')

    tb_logger = SummaryWriter(log_dir='./tb_logger/' + args.experiment)

    if not os.path.exists(os.path.join('experiments', args.experiment, 'checkpoints')):
        os.makedirs(os.path.join('experiments', args.experiment, 'checkpoints'))
    # train_transforms = transforms.Compose(
    #     [transforms.RandomCrop(args.patch_size), transforms.ToTensor()]
    # )

    # test_transforms = transforms.Compose(
    #     [transforms.ToTensor()]
    # )

    train_dataset = FeatureFolder(args.dataset, split="", mode="train_all", rate=args.rate)#, transform=train_transforms)
    test_dataset = FeatureFolder(args.test_dataset, split="", mode="all_with_size")#, transform=test_transforms)

    # device = "cuda" if args.cuda and torch.cuda.is_available() else "cpu"
    device = "cuda" if args.cuda!=-1 and torch.cuda.is_available() else "cpu"
    if device == 'cuda':
        torch.cuda.set_device(args.cuda)

    train_dataloader = DataLoader(
        train_dataset,
        batch_size=args.batch_size,
        num_workers=args.num_workers,
        shuffle=True,
        pin_memory=(device == "cuda"),
    )

    test_dataloader = DataLoader(
        test_dataset,
        batch_size=1,#args.test_batch_size,
        num_workers=args.num_workers,
        shuffle=False,
        pin_memory=(device == "cuda"),
    )

    # net = image_models[args.model](quality=args.quality)
    net = ScaleHyperprior4(M = 192, N = 128) #M=192, N=128,  320,192

    net = net.to(device)
    IRN_net = InvRescaleNet(4, 4, subnet("DBNet", 'xavier'), [8], 1)
    # IRN_net = InvRescaleNet(256, 256, subnet("DBNet", 'xavier'), [8], 1)
    IRN_net = IRN_net.to(device)
    En_net = ARCNNModel()

    logger_train.info(args)
    logger_train.info(net)
    logger_train.info(IRN_net)
    logger_train.info(En_net)

    # Load IRN
    IRN_path = args.irncheckpoint or "./experiments/exp_cheng_En_01_only_q4/checkpoints/IRN_net_checkpoint_best_loss.pth.tar"
    try:
        print("IRN path:{}".format(IRN_path))
        checkpoint = torch.load(IRN_path, map_location=lambda storage, loc: storage)
        IRN_net.load_state_dict(checkpoint['state_dict'])
    except:
        try:
            load_IRN(IRN_path, IRN_net) #official
            print("IRN ori")
        except:
            print("IRN load none")

    optimizer, aux_optimizer = configure_optimizers(net, args)
    optimizer_IRN = Net_optimizers(IRN_net, args.learning_rate)
    optimizer_En = Net_optimizers(En_net, args.learning_rate)

    # lr_scheduler = optim.lr_scheduler.ReduceLROnPlateau(optimizer, "min")
    lr_scheduler = optim.lr_scheduler.MultiStepLR(optimizer, milestones=[2000, 4250], gamma=0.5)
    lr_scheduler_IRN = optim.lr_scheduler.MultiStepLR(optimizer_IRN, milestones=[2000, 4250], gamma=0.5)
    lr_scheduler_En = optim.lr_scheduler.MultiStepLR(optimizer_En, milestones=[2000, 4250], gamma=0.5)

    criterion = RateDistortionLoss(lmbda=args.lmbda, mode=args.mode)

    last_epoch = 0
    if args.checkpoint:  # load from previous checkpoint
        print("Loading codec:", args.checkpoint)
        # checkpoint = torch.load(args.checkpoint, map_location=device)
        # last_epoch = checkpoint["epoch"] + 1
        # net.load_state_dict(checkpoint)
        # net.load_state_dict(checkpoint["state_dict"])
        # optimizer.load_state_dict(checkpoint["optimizer"])
        # aux_optimizer.load_state_dict(checkpoint["aux_optimizer"])
        # lr_scheduler.load_state_dict(checkpoint["lr_scheduler"])

        checkpoint = torch.load(args.checkpoint, map_location=lambda storage, loc: storage)

        try:
            net.load_state_dict(checkpoint['state_dict'])
        except:
            try:
                for key in list(checkpoint):
                    if key.split(".")[0] == "entropy_bottleneck":
                        if key.split(".")[1] == "_biases":
                            checkpoint[key.split(".")[0] + '._bias' + key.split(".")[2]] = checkpoint[key]
                            del (checkpoint[key])
                        if key.split(".")[1] == "_factors":
                            checkpoint[key.split(".")[0] + '._factor' + key.split(".")[2]] = checkpoint[key]
                            del (checkpoint[key])
                        if key.split(".")[1] == "_matrices":
                            checkpoint[key.split(".")[0] + '._matrix' + key.split(".")[2]] = checkpoint[key]
                            del (checkpoint[key])
                net.load_state_dict(checkpoint)
                print("load codec ori")
            except:
                print("codec load none")

        ## ywz end
        # checkpoint.keys()
        # new_dict = net.state_dict()
        # pretrained_dict = {k: v for k, v in checkpoint.items() if
        #                    (k in new_dict)}  # filter out unnecessary keys
        # new_dict.update(pretrained_dict)
        # net.load_state_dict(new_dict)

    if args.onlytest:
        print("only test")
        loss = test_epoch(0, test_dataloader, net, IRN_net, En_net, criterion, logger_val, tb_logger)
        return 

    best_loss = float("inf")
    for epoch in range(last_epoch, args.epochs):
        # print(f"Learning rate: {optimizer.param_groups[0]['lr']}")
        logger_train.info(f"Learning rate: {optimizer.param_groups[0]['lr']}")
        train_one_epoch(
            net,
            IRN_net,
            En_net,
            criterion,
            train_dataloader,
            optimizer,
            optimizer_IRN,
            optimizer_En,
            aux_optimizer,
            epoch,
            args.clip_max_norm,
            logger_train,
            tb_logger
        )
        if epoch % 2 == 0:
            loss = test_epoch(epoch, test_dataloader, net, IRN_net, En_net, criterion, logger_val, tb_logger)

        lr_scheduler.step()
        lr_scheduler_IRN.step()
        lr_scheduler_En.step()

        is_best = loss < best_loss
        best_loss = min(loss, best_loss)

        if args.save:
            save_checkpoint(
                {
                    "state_dict": net.state_dict(),
                },
                is_best,
                "net",
                os.path.join('experiments', args.experiment, 'checkpoints', "net_checkpoint.pth.tar")
            )
            save_checkpoint(
                {
                    "state_dict": IRN_net.state_dict(),
                },
                is_best,
                "IRN_net",
                os.path.join('experiments', args.experiment, 'checkpoints',
                             "IRN_net_checkpoint.pth.tar")
            )
            save_checkpoint(
                {
                    "state_dict": En_net.state_dict(),
                },
                is_best,
                "En_net",
                os.path.join('experiments', args.experiment, 'checkpoints',
                             "En_net_checkpoint.pth.tar")
            )
            if is_best:
                logger_val.info('best checkpoint saved.')


if __name__ == "__main__":
    main(sys.argv[1:])
