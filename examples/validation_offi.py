# python3 examples/train.py -m cheng2020-attn -d /media/disk2/jjp/jjp/Dataset/DIV2K/DIV2K_train_HR/ -d_test /media/disk2/jjp/jjp/Dataset/div_after_crop/ -q 4 --lambda 0.001 --batch-size 6 -lr 1e-5 --save --cuda --exp exp_cheng_En_01_only_q4 --checkpoint /home/jjp/CompressAI/pretrained_model/cheng2020-attn/cheng2020_attn-ms-ssim-4-8b2f647e.pth.tar
# ywz
# python3 examples/validation.py -m bmshj2018-hyperprior -d /media/data/yangwenzhe/rm_0_channels/ -q 4 --lambda 0.01 --batch-size 1 -lr 1e-4 --save --cuda --exp exp_ywz_balle_En_03_only_q4 --checkpoint /media/data/yangwenzhe/ywzCompressAI/pretrained_model/bmshj2018-hyperprior/q4/bmshj2018-hyperprior-4-de1b779c.pth.tar --en /media/data/yangwenzhe/ywzCompressAI/pretrained_model/ARCNN/ckp_first_best.pt

import os
import argparse
import math
import random
import shutil
import sys

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision

from torch.utils.data import DataLoader
from torchvision import transforms
from collections import OrderedDict

import logging
from compressai.datasets.parse_p2_feature import fea2png, save_png
from utils import util
from compressai.datasets import ImageFolder, FeatureFolder
from compressai.zoo import image_models
from compressai.models.Inv_arch import InvRescaleNet
from compressai.models.Subnet_constructor import subnet
from compressai.models.Enhance import EnModule
from compressai.models.ARCNN import ARCNNModel
from torchvision.transforms import ToPILImage
import numpy as np
import PIL
import PIL.Image as Image
from torchvision.transforms import ToPILImage
from pytorch_msssim import ms_ssim
from typing import Tuple, Union
from torch.utils.tensorboard import SummaryWriter
import torch.nn.functional as F


def torch2img(x: torch.Tensor) -> Image.Image:
    return ToPILImage()(x.cpu().clamp_(0, 1).squeeze())

def padding_size(ori_size, factor_size):
    if ori_size % factor_size == 0:
        return ori_size
    else:
        return factor_size * (ori_size // factor_size + 1)


def compute_metrics(
        a: Union[np.array, Image.Image],
        b: Union[np.array, Image.Image],
        max_val: float = 255.0,
) -> Tuple[float, float]:
    """Returns PSNR and MS-SSIM between images `a` and `b`. """
    if isinstance(a, Image.Image):
        a = np.asarray(a)
    if isinstance(b, Image.Image):
        b = np.asarray(b)

    a = torch.from_numpy(a.copy()).float().unsqueeze(0)
    if a.size(3) == 3:
        a = a.permute(0, 3, 1, 2)
    b = torch.from_numpy(b.copy()).float().unsqueeze(0)
    if b.size(3) == 3:
        b = b.permute(0, 3, 1, 2)

    mse = torch.mean((a - b) ** 2).item()
    p = 20 * np.log10(max_val) - 10 * np.log10(mse)
    # m = ms_ssim(a, b, data_range=max_val).item()
    m = 0
    return p, m


class RateDistortionLoss(nn.Module):
    """Custom rate distortion loss with a Lagrangian parameter."""

    def __init__(self, lmbda=1e-2):
        super().__init__()
        self.mse = nn.MSELoss()
        self.lmbda = lmbda

    def forward(self, output, target, lq, x_l, x_enh):
        N, _, H, W = target.size()
        out = {}
        num_pixels = N * H * W

        out["bpp_loss"] = sum(
            (torch.log(likelihoods).sum() / (-math.log(2) * num_pixels))
            for likelihoods in output["likelihoods"].values()
        )
        out["mse_loss"] = self.mse(lq, target)
        # out["loss"] = self.lmbda * 255 ** 2 * out["mse_loss"] + out["bpp_loss"]
        out["loss"] = self.mse(x_l, x_enh)
        # out["loss"] = self.mse(x_l, x_enh) + out["mse_loss"]
        return out


class AverageMeter:
    """Compute running average."""

    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class CustomDataParallel(nn.DataParallel):
    """Custom DataParallel to access the module methods."""

    def __getattr__(self, key):
        try:
            return super().__getattr__(key)
        except AttributeError:
            return getattr(self.module, key)


def configure_optimizers(net, args):
    """Separate parameters for the main optimizer and the auxiliary optimizer.
    Return two optimizers"""

    parameters = {
        n
        for n, p in net.named_parameters()
        if not n.endswith(".quantiles") and p.requires_grad
    }
    aux_parameters = {
        n
        for n, p in net.named_parameters()
        if n.endswith(".quantiles") and p.requires_grad
    }

    # Make sure we don't have an intersection of parameters
    params_dict = dict(net.named_parameters())
    inter_params = parameters & aux_parameters
    union_params = parameters | aux_parameters

    assert len(inter_params) == 0
    assert len(union_params) - len(params_dict.keys()) == 0

    optimizer = optim.Adam(
        (params_dict[n] for n in sorted(parameters)),
        lr=args.learning_rate,
    )
    aux_optimizer = optim.Adam(
        (params_dict[n] for n in sorted(aux_parameters)),
        lr=args.aux_learning_rate,
    )
    return optimizer, aux_optimizer


def Net_optimizers(net, learning_rate):
    parameters = {
        n for n, p in net.named_parameters() if p.requires_grad
    }

    params_dict = dict(net.named_parameters())

    optimizer = optim.Adam(
        (params_dict[n] for n in sorted(parameters)),
        lr=learning_rate,
    )

    return optimizer


def test_epoch(epoch, test_dataloader, model, IRN_net, En_net, criterion, logger_val, tb_logger):
    model.eval()
    device = next(model.parameters()).device

    loss = AverageMeter()
    bpp_loss = AverageMeter()
    mse_loss = AverageMeter()
    aux_loss = AverageMeter()
    psnr = AverageMeter()
    ms_ssim = AverageMeter()

    psnr_1 = AverageMeter()
    psnr_2 = AverageMeter()
    # i = 0
    with torch.no_grad():
        for temp_data in test_dataloader:
            p2,p3,p4,p5, temp_path = temp_data[0], temp_data[1], temp_data[2], temp_data[3], temp_data[4]
            temp_feat = [p2, p3, p4, p5]
            # p2, temp_path = temp_data[0], temp_data[1]
            # temp_feat = [p2]
            # for temp_feat_idx in range(len(temp_feat)):
            for temp_feat_idx in [0]:
                d_ori = temp_feat[temp_feat_idx]
                # raise ValueError("d_ori min:{}, max:{}".format(d_ori.min(), d_ori.max()))
                d_ori = (d_ori + 12.5)/25
                # d_ori = d_ori.to(device)
                # ywz add 
                # temp_ori_size = d_ori.shape
                # target_size = [d_ori.size()[0], padding_size(d_ori.size()[1], 3), padding_size(d_ori.size()[2], 128), padding_size(d_ori.size()[3], 128)]
                temp_ori_size = d_ori.shape
                target_size = [d_ori.size()[0], d_ori.size()[1], padding_size(d_ori.size()[2], 128), padding_size(d_ori.size()[3], 128)]
                # raise ValueError("{}->{}".format(temp_ori_size, target_size))
                d_big = torch.zeros(target_size)
                d_big[:,0:temp_ori_size[1], 0:temp_ori_size[2], 0:temp_ori_size[3]] = d_ori
                d_out_merge = torch.zeros(temp_ori_size)
                
                # for channel_idx in range(d_big.size()[1] // 3):
                for channel_idx in range(d_big.size()[1]):
                    # d = d_big[:, channel_idx*3 : (channel_idx+1)*3 ,:,:]
                    # copy 1 to 3
                    d = d_big[:, channel_idx : (channel_idx+1) ,:,:]
                    d = d.repeat(1, 3, 1, 1) #channel x 3
                    d = d.to(device)
                    # interface
                    x_l = IRN_inference(IRN_net, d, device)
                    out_net = model(x_l)
                    # x_enh = En_net(out_net["x_hat"])
                    x_enh = out_net["x_hat"]
                    x_hat = IRN_inference(IRN_net, x_enh, device, rev=True)
                    # raise ValueError("shape,{},{},{},{}".format(d.shape,x_l.shape, x_enh.shape, x_hat.shape))
                    # shape,torch.Size([1, 3, 256, 384]),torch.Size([1, 3, 128, 192]),torch.Size([1, 3, 128, 192]),torch.Size([1, 3, 256, 384])
                    out_criterion = criterion(out_net, d, x_hat, x_l, x_enh)
                    # 
                    # print("d shape {}, x_l:{}, x_enh:{}, x_hat:{}, d_out_merge:{}".format(d.size(), x_l.size(), x_enh.size(), x_hat.size(), d_out_merge.size()))
                    aux_loss.update(model.aux_loss())
                    bpp_loss.update(out_criterion["bpp_loss"])
                    loss.update(out_criterion["loss"])
                    mse_loss.update(out_criterion["mse_loss"])
                    # done and merge
                    d_out = x_hat #TODO
                    d_out = d_out * 25 - 12.5
                    # d_out = d # TODO copy directly
                    # raise ValueError("stop to check: d_in:{},\n d_out:{}".format(d, d_out))
                    # d_out_merge[:,channel_idx*3 : min(temp_ori_size[1], (channel_idx+1)*3),:,:] = d_out[:, 0: (min(temp_ori_size[1], (channel_idx+1)*3) - channel_idx*3),0:temp_ori_size[2],0:temp_ori_size[3]]
                    d_out_merge[:,channel_idx: (channel_idx+1),:,:] = d_out[:, 0: 1,0:temp_ori_size[2],0:temp_ori_size[3]]

                temp_feat[temp_feat_idx] = d_out_merge
                # raise ValueError("success. d_out_merge size:".format(d_out_merge.size()))
                # ywz done

            # save_dir = "./experiments/exp_test/images_quant/"
            save_dir = "./experiments/exp_test/test/"
            # save ywz
            print("file:{} , bpp till now:{}".format(temp_path[0].split(".")[-2].split("/")[-1], bpp_loss.avg))
            features_png = fea2png(feat= [temp_feat_idx.squeeze() for temp_feat_idx in temp_feat])
            # raise ValueError(features_png)
            save_png(features_png, temp_path[0].split(".")[-2].split("/")[-1], save_dir, "single")
            # done ywz
            # raise ValueError("success.")
            # i = i + 1
            # print(i)

    tb_logger.add_scalar('{}'.format('[val]: loss'), loss.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: bpp_loss'), bpp_loss.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: psnr'), psnr.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: dpsnr'), psnr_2.avg - psnr_1.avg, epoch + 1)
    
    # logger_val.info(
    print(
        f"Test epoch {epoch}: Average losses: "
        f"Loss: {loss.avg:.4f} | "
        f"MSE loss: {mse_loss.avg:.4f} | "
        f"Bpp loss: {bpp_loss.avg:.4f} | "
        f"Aux loss: {aux_loss.avg:.2f} | "
        f"PSNR: {psnr.avg:.6f} | "
        f"MS-SSIM: {ms_ssim.avg:.6f} | "
        f"PSNR_1: {psnr_1.avg:.6f} | "
        f"PSNR_2: {psnr_2.avg:.6f} | "
        f"BPP_SUM: {bpp_loss.sum:.6f} | "
    )
    return loss.avg


def IRN_inference(model, input, device, rev=False):
    if rev:
        b, c, h, w = input.shape
        z = torch.randn([b, 3 * c, h, w]).to(device)
        out = model(torch.cat((input, z), 1), rev)

    else:
        x_forward = model(input)
        out = x_forward[:, :3, :, :]

    return out


def save_checkpoint(state, is_best, suffix, filename="checkpoint.pth.tar"):
    torch.save(state, filename)
    if is_best:
        dest_filename = filename.replace(filename.split('/')[-1], suffix + "_checkpoint_best_loss.pth.tar")
        shutil.copyfile(filename, dest_filename)


def load_ARCNN(load_path, network, strict=True):
    if isinstance(network, nn.DataParallel):
        network = network.module
    load_net = torch.load(load_path)
    load_net_clean = OrderedDict()  # remove unnecessary 'module.'
    for k, v in load_net['network']['net'].items():
        if k.startswith('module.'):
            load_net_clean[k[7:]] = v
        else:
            load_net_clean[k] = v
    network.load_state_dict(load_net_clean, strict=strict)


def load_IRN(path, network):
    load_path = path
    load_network(load_path, network, True)


def load_network(load_path, network, strict=True):
    if isinstance(network, nn.DataParallel):
        network = network.module
    load_net = torch.load(load_path)

    load_net_clean = OrderedDict()  # remove unnecessary 'module.'
    for k, v in load_net.items():
        if k.startswith('module.'):
            load_net_clean[k[7:]] = v
        else:
            load_net_clean[k] = v
    network.load_state_dict(load_net_clean, strict=strict)


def load_En(checkpoint, net):
    state_dicts = torch.load(checkpoint)
    network_state_dict = {k: v for k, v in state_dicts['net'].items() if 'tmp_var' not in k}
    net.load_state_dict(network_state_dict)


def parse_args(argv):
    parser = argparse.ArgumentParser(description="Example training script.")
    parser.add_argument(
        "-m",
        "--model",
        default="bmshj2018-factorized",
        choices=image_models.keys(),
        help="Model architecture (default: %(default)s)",
    )
    # parser.add_argument(
    #     "-d", "--dataset", type=str, required=True, help="Training dataset"
    # )
    parser.add_argument(
        "-d_test", "--test_dataset", type=str, required=True, help="Training dataset"
    )
    parser.add_argument(
        "-e",
        "--epochs",
        default=5000,
        type=int,
        help="Number of epochs (default: %(default)s)",
    )
    parser.add_argument(
        "-lr",
        "--learning-rate",
        default=1e-4,
        type=float,
        help="Learning rate (default: %(default)s)",
    )
    parser.add_argument(
        "-n",
        "--num-workers",
        type=int,
        default=4,
        help="Dataloaders threads (default: %(default)s)",
    )
    parser.add_argument(
        "-q",
        "--quality",
        type=int,
        default=1,
    )
    parser.add_argument(
        "--lambda",
        dest="lmbda",
        type=float,
        default=1e-2,
        help="Bit-rate distortion parameter (default: %(default)s)",
    )
    parser.add_argument(
        "--batch-size", type=int, default=16, help="Batch size (default: %(default)s)"
    )
    parser.add_argument(
        "--test-batch-size",
        type=int,
        default=1,
        help="Test batch size (default: %(default)s)",
    )
    parser.add_argument(
        "--aux-learning-rate",
        default=1e-3,
        help="Auxiliary loss learning rate (default: %(default)s)",
    )
    parser.add_argument(
        "--patch-size",
        type=int,
        nargs=2,
        default=(256, 256),
        help="Size of the patches to be cropped (default: %(default)s)",
    )
    # parser.add_argument("--cuda", action="store_true", help="Use cuda")
    parser.add_argument(
        "--cuda",
        type=int,
        default=-1,
        help="Use cuda",
    )
    parser.add_argument(
        "--save", action="store_true", default=True, help="Save model to disk"
    )
    parser.add_argument(
        "--seed", type=float, help="Set random seed for reproducibility"
    )
    parser.add_argument(
        "--clip_max_norm",
        default=1.0,
        type=float,
        help="gradient clipping max norm (default: %(default)s",
    )
    parser.add_argument(
        "-exp", "--experiment", type=str, required=True, help="Experiment name"
    )
    parser.add_argument("--irncheckpoint", type=str, help="Path to a irncheckpoint")
    parser.add_argument("--checkpoint", type=str, help="Path to a checkpoint")
    parser.add_argument("--en", type=str, help="Path to the EN checkpoint")
    args = parser.parse_args(argv)
    return args


def main(argv):
    args = parse_args(argv)

    if args.seed is not None:
        torch.manual_seed(args.seed)
        random.seed(args.seed)

    if not os.path.exists(os.path.join('experiments', args.experiment)):
        os.makedirs(os.path.join('experiments', args.experiment))

    print(args.experiment)
    # util.setup_logger('train', os.path.join('experiments', args.experiment), 'train_' + args.experiment,
    #                   level=logging.INFO,
    #                   screen=True, tofile=True)
    util.setup_logger('val', os.path.join('experiments', args.experiment), 'val_' + args.experiment,
                      level=logging.INFO,
                      screen=True, tofile=True)

    logger_train = logging.getLogger('train')
    logger_val = logging.getLogger('val')

    tb_logger = SummaryWriter(log_dir='./tb_logger/' + args.experiment)

    if not os.path.exists(os.path.join('experiments', args.experiment, 'checkpoints')):
        os.makedirs(os.path.join('experiments', args.experiment, 'checkpoints'))
    # train_transforms = transforms.Compose(
    #     [transforms.RandomCrop(args.patch_size), transforms.ToTensor()]
    # )

    test_transforms = transforms.Compose(
        [transforms.ToTensor()]
    )

    # test_dataset = ImageFolder(args.test_dataset, split="", transform=test_transforms)
    test_dataset = FeatureFolder(args.test_dataset, split="", mode="test")#, transform=test_transforms)

    device = "cuda" if args.cuda!=-1 and torch.cuda.is_available() else "cpu"
    if device == 'cuda':
        torch.cuda.set_device(args.cuda)

    test_dataloader = DataLoader(
        test_dataset,
        batch_size=args.test_batch_size,
        num_workers=args.num_workers,
        shuffle=False,
        pin_memory=("cuda" in device),
    )

    net = image_models[args.model](quality=args.quality)
    net = net.to(device)
    IRN_net = InvRescaleNet(3, 3, subnet("DBNet", 'xavier'), [8], 1)
    IRN_net = IRN_net.to(device)
    # En_net = ARCNNModel()
    En_net = EnModule(64, 3, 16, 10, 5)
    En_net = En_net.to(device)

    # if args.cuda and torch.cuda.device_count() > 1:
        # net = CustomDataParallel(net)
        # IRN_net = CustomDataParallel(IRN_net)
        # En_net = CustomDataParallel(En_net)

    logger_train.info(args)
    logger_train.info(net)
    logger_train.info(IRN_net)
    logger_train.info(En_net)

    # Load IRN
    # IRN_path = "./experiments/exp_cheng_En_01_only_q4/checkpoints/IRN_net_checkpoint_best_loss.pth.tar"
    # checkpoint = torch.load(IRN_path, map_location=lambda storage, loc: storage)
    # IRN_net.load_state_dict(checkpoint['state_dict'])

    # Load EN
    # # EN_path = "./experiments/exp_cheng_En_02_only_q3/checkpoints/En_net_checkpoint_best_loss.pth.tar"
    # EN_path = args.en
    # print("Loading", args.en)
    # checkpoint = torch.load(EN_path, map_location=lambda storage, loc: storage)
    # En_net.load_state_dict(checkpoint['state_dict'])
    # # EN_path = "/home/jjp/CompressAI/pretrained_model/ARCNN/ckp_first_best.pt"
    # # load_ARCNN(EN_path, En_net)

    IRN_path = args.irncheckpoint or "/media/data/yangwenzhe/ywzCompressAI/pretrained_model/IRN/x2/IRN_x2.pth"
    #
    try:
        checkpoint = torch.load(IRN_path, map_location=lambda storage, loc: storage)
        IRN_net.load_state_dict(checkpoint['state_dict'])
    except:
        print("load ori IRN")
        load_IRN(IRN_path, IRN_net) #official

    # temp_epoch(test_dataloader, IRN_net)

    criterion = RateDistortionLoss(lmbda=args.lmbda)

    last_epoch = 0
    if args.checkpoint:  # load from previous checkpoint
        print("Loading", args.checkpoint)
        # checkpoint = torch.load(args.checkpoint, map_location=device)
        # last_epoch = checkpoint["epoch"] + 1
        # net.load_state_dict(checkpoint)
        # net.load_state_dict(checkpoint["state_dict"])
        # optimizer.load_state_dict(checkpoint["optimizer"])
        # aux_optimizer.load_state_dict(checkpoint["aux_optimizer"])
        # lr_scheduler.load_state_dict(checkpoint["lr_scheduler"])

        checkpoint = torch.load(args.checkpoint, map_location=lambda storage, loc: storage)

        for key in list(checkpoint):
            if key.split(".")[0] == "entropy_bottleneck":
                if key.split(".")[1] == "_biases":
                    checkpoint[key.split(".")[0] + '._bias' + key.split(".")[2]] = checkpoint[key]
                    del (checkpoint[key])
                if key.split(".")[1] == "_factors":
                    checkpoint[key.split(".")[0] + '._factor' + key.split(".")[2]] = checkpoint[key]
                    del (checkpoint[key])
                if key.split(".")[1] == "_matrices":
                    checkpoint[key.split(".")[0] + '._matrix' + key.split(".")[2]] = checkpoint[key]
                    del (checkpoint[key])
        net.load_state_dict(checkpoint)
        # ^
        # net.load_state_dict(checkpoint['state_dict'])
        # -
        # checkpoint.keys()
        # new_dict = net.state_dict()
        # pretrained_dict = {k: v for k, v in checkpoint.items() if
        #                    (k in new_dict)}  # filter out unnecessary keys
        # new_dict.update(pretrained_dict)
        # net.load_state_dict(new_dict)

        # logger_train.info(f"Learning rate: {optimizer.param_groups[0]['lr']}")

        loss = test_epoch(0, test_dataloader, net, IRN_net, En_net, criterion, logger_val, tb_logger)

if __name__ == "__main__":
    main(sys.argv[1:])
