# python3 examples/train.py -m cheng2020-attn -d /media/disk2/jjp/jjp/Dataset/DIV2K/DIV2K_train_HR/ -d_test /media/disk2/jjp/jjp/Dataset/div_after_crop/ -q 4 --lambda 0.001 --batch-size 6 -lr 1e-5 --save --cuda --exp exp_cheng_En_01_only_q4 --checkpoint /home/jjp/CompressAI/pretrained_model/cheng2020-attn/cheng2020_attn-ms-ssim-4-8b2f647e.pth.tar
# ywz
# python3 examples/validation.py -m bmshj2018-hyperprior -d /media/data/yangwenzhe/rm_0_channels/ -q 4 --lambda 0.01 --batch-size 1 -lr 1e-4 --save --cuda --exp exp_ywz_balle_En_03_only_q4 --checkpoint /media/data/yangwenzhe/ywzCompressAI/pretrained_model/bmshj2018-hyperprior/q4/bmshj2018-hyperprior-4-de1b779c.pth.tar --en /media/data/yangwenzhe/ywzCompressAI/pretrained_model/ARCNN/ckp_first_best.pt

import os
import argparse
import math
import random
import shutil
import sys

import torch
import torch.nn as nn
import torch.optim as optim
import torchvision

from torch.utils.data import DataLoader
from torchvision import transforms
from collections import OrderedDict

import logging
from compressai.datasets.parse_p2_feature import fea2png, save_png
from utils import util
from compressai.datasets import ImageFolder, FeatureFolder
from compressai.zoo import image_models
from compressai.models.Inv_arch import InvRescaleNet
from compressai.models.Subnet_constructor import subnet
from compressai.models.Enhance import EnModule
from compressai.models.ARCNN import ARCNNModel
from torchvision.transforms import ToPILImage
import numpy as np
import PIL
import PIL.Image as Image
from torchvision.transforms import ToPILImage
from pytorch_msssim import ms_ssim
from typing import Tuple, Union
from torch.utils.tensorboard import SummaryWriter
import torch.nn.functional as F
from compressai.models import *


irn_channels_num = 4

def torch2img(x: torch.Tensor) -> Image.Image:
    return ToPILImage()(x.cpu().clamp_(0, 1).squeeze())

def padding_size(ori_size, factor_size):
    if ori_size % factor_size == 0:
        return ori_size
    else:
        return factor_size * (ori_size // factor_size + 1)

def mse2psnr(mse):
    # 根据Hyper论文中的内容，将MSE->psnr(db)
    # return 10*math.log10(255*255/mse)
    return 10 * math.log10(1/ mse)
def compute_metrics(
        a: Union[np.array, Image.Image],
        b: Union[np.array, Image.Image],
        max_val: float = 255.0,
) -> Tuple[float, float]:
    """Returns PSNR and MS-SSIM between images `a` and `b`. """
    if isinstance(a, Image.Image):
        a = np.asarray(a)
    if isinstance(b, Image.Image):
        b = np.asarray(b)

    a = torch.from_numpy(a.copy()).float().unsqueeze(0)
    if a.size(3) == 3:
        a = a.permute(0, 3, 1, 2)
    b = torch.from_numpy(b.copy()).float().unsqueeze(0)
    if b.size(3) == 3:
        b = b.permute(0, 3, 1, 2)

    mse = torch.mean((a - b) ** 2).item()
    p = 20 * np.log10(max_val) - 10 * np.log10(mse)
    # m = ms_ssim(a, b, data_range=max_val).item()
    m = 0
    return p, m


class RateDistortionLoss(nn.Module):
    """Custom rate distortion loss with a Lagrangian parameter."""

    def __init__(self, lmbda=1e-2):
        super().__init__()
        self.mse = nn.MSELoss()
        self.lmbda = lmbda

    def forward(self, output=None, target=None, lq=None, x_l=None, x_enh=None, H_in=None, W_in=None):
        N, _, H, W = target.size()
        H = H_in if H_in else H
        W = W_in if W_in else W
        out = {}
        num_pixels = N * H * W

        if output:
            out["bpp_loss"] = sum(
                (torch.log(likelihoods).sum() / (-math.log(2) * num_pixels))
                for likelihoods in output["likelihoods"].values()
            )
            out["mse_loss"] = self.mse(lq, target)
            out["loss"] = self.lmbda * 255 ** 2 * out["mse_loss"] + out["bpp_loss"]
        

        else:
            out["mse_loss"] = self.mse(lq, target)
        # out["loss"] = self.mse(x_l, x_enh)
        # out["loss"] = self.mse(x_l, x_enh) + out["mse_loss"]
        return out


class AverageMeter:
    """Compute running average."""

    def __init__(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count


class CustomDataParallel(nn.DataParallel):
    """Custom DataParallel to access the module methods."""

    def __getattr__(self, key):
        try:
            return super().__getattr__(key)
        except AttributeError:
            return getattr(self.module, key)


def configure_optimizers(net, args):
    """Separate parameters for the main optimizer and the auxiliary optimizer.
    Return two optimizers"""

    parameters = {
        n
        for n, p in net.named_parameters()
        if not n.endswith(".quantiles") and p.requires_grad
    }
    aux_parameters = {
        n
        for n, p in net.named_parameters()
        if n.endswith(".quantiles") and p.requires_grad
    }

    # Make sure we don't have an intersection of parameters
    params_dict = dict(net.named_parameters())
    inter_params = parameters & aux_parameters
    union_params = parameters | aux_parameters

    assert len(inter_params) == 0
    assert len(union_params) - len(params_dict.keys()) == 0

    optimizer = optim.Adam(
        (params_dict[n] for n in sorted(parameters)),
        lr=args.learning_rate,
    )
    aux_optimizer = optim.Adam(
        (params_dict[n] for n in sorted(aux_parameters)),
        lr=args.aux_learning_rate,
    )
    return optimizer, aux_optimizer


def Net_optimizers(net, learning_rate):
    parameters = {
        n for n, p in net.named_parameters() if p.requires_grad
    }

    params_dict = dict(net.named_parameters())

    optimizer = optim.Adam(
        (params_dict[n] for n in sorted(parameters)),
        lr=learning_rate,
    )

    return optimizer


def _tensor_joint(temp_feat, channel_factor = 3, pixel_factor = 128):
    # temp_feat = [p2,p3,p4,p5]
    ch_factor_res = 0
    for ii in range(len(temp_feat)):
        ch_factor_res += 1/(4**ii)
    out_channels = (int)(ch_factor_res*temp_feat[0].shape[1]) #长宽都是2倍2倍降
    temp_tensor = torch.zeros(temp_feat[0].shape[0], padding_size(out_channels, channel_factor), padding_size(temp_feat[0].shape[2], pixel_factor), padding_size(temp_feat[0].shape[3], pixel_factor))
    channel_start = 0
    for ii in range(len(temp_feat)):
        size_num = 2**ii
        delta_channel_num = temp_feat[0].shape[1] // (size_num * size_num)
        channel_end = channel_start + delta_channel_num
        little_row = temp_feat[ii].shape[2]
        little_col = temp_feat[ii].shape[3]
        channel_idx = 0
        for row in range(size_num):
            for col in range(size_num):
                temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)] = \
                    temp_feat[ii][:, channel_idx:(channel_idx + delta_channel_num),:,:]
                channel_idx += delta_channel_num
        channel_start = channel_end
    return temp_tensor

def _tensor_split(temp_tensor, ori_feat0_shape, channel_factor = 3):
    temp_feat = []
    len_temp_feat = 0
    temp_channels = temp_tensor.shape[1]
    cnt_channels = ori_feat0_shape[1]
    # print("ori channels:{}".format(cnt_channels))
    while temp_channels >= padding_size(cnt_channels, channel_factor):
        len_temp_feat += 1
        if (temp_channels == padding_size(cnt_channels, channel_factor)):
            break
        cnt_channels += ori_feat0_shape[1] * (1/(4**len_temp_feat))
    ##
    # print("len feat:{}".format(len_temp_feat))
    channel_start = 0
    for ii in range(len_temp_feat):
        size_num = 2**ii
        little_row = ori_feat0_shape[2] // size_num
        little_col = ori_feat0_shape[3] // size_num
        temp_rev_tensor = torch.zeros(ori_feat0_shape[0], ori_feat0_shape[1], little_row, little_col)
        delta_channel_num = ori_feat0_shape[1] // (size_num * size_num)
        channel_end = channel_start + delta_channel_num
        channel_idx = 0
        ##
        for row in range(size_num):
            for col in range(size_num):
                temp_rev_tensor[:, channel_idx:(channel_idx + delta_channel_num),:,:] = \
                    temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)]
                channel_idx += delta_channel_num
        channel_start = channel_end
        ##
        temp_feat.append(temp_rev_tensor)
    return temp_feat

def test_epoch(epoch, test_dataloader, model, IRN_net, En_net, criterion, logger_val, tb_logger):
    model.eval()
    IRN_net.eval()
    device = next(model.parameters()).device

    loss = AverageMeter()
    bpp_loss = AverageMeter()
    mse_loss = AverageMeter()
    aux_loss = AverageMeter()
    psnr = AverageMeter()
    ms_ssim = AverageMeter()

    psnr_1 = AverageMeter()
    psnr_2 = AverageMeter()
    # i = 0
    with torch.no_grad():
        for i,temp_data in enumerate(test_dataloader):
            p2,p3,p4,p5, temp_path, jpg_size = temp_data[0], temp_data[1], temp_data[2], temp_data[3], temp_data[4], temp_data[5]
            # jpg_pixels = jpg_size[0] * jpg_size[1]
            # padding for p3~p5
            d_p2 = _tensor_joint([p2], channel_factor=4, pixel_factor=32).to(device) #cheng原版模型为64整数倍 -- p2 单独过IRN
            d_p3 = _tensor_joint([p3], channel_factor=4, pixel_factor=16).to(device) #cheng原版模型为64整数倍
            d_p4 = _tensor_joint([p4], channel_factor=4, pixel_factor=16).to(device) #cheng原版模型为64整数倍
            d_p5 = _tensor_joint([p5], channel_factor=4, pixel_factor=16).to(device) #cheng原版模型为64整数倍
            # raise ValueError("stop {},{}".format(p2.shape, d_p2.shape))
            
            irn_target_size = [p2.size()[0], p2.size()[1], d_p2.shape[2]//2, d_p2.shape[3]//2]
            irn_target_merge = torch.zeros(irn_target_size).to(device) # after IRN
            irn_target_merge_lq = torch.zeros(irn_target_size).to(device) # after codec
            irn_res_merge = torch.zeros_like(d_p2).to(device) # after IRN REV

            d_p3_lq = torch.zeros_like(d_p3).to(device) # after codec
            d_p4_lq = torch.zeros_like(d_p4).to(device) # after codec
            d_p5_lq = torch.zeros_like(d_p5).to(device) # after codec
            # raise ValueError("stop {},{}".format(d_p2.shape, irn_res_merge.shape))

            # change 统一预处理补边
            d_p2 = (d_p2 + 23.1728) / 43.6082
            d_p3 = (d_p3 + 23.1728) / 43.6082
            d_p4 = (d_p4 + 23.1728) / 43.6082
            d_p5 = (d_p5 + 23.1728) / 43.6082
            irn_target_merge = (irn_target_merge + 23.1728) / 43.6082
            irn_target_merge_lq = (irn_target_merge_lq + 23.1728) / 43.6082
            irn_res_merge = (irn_res_merge + 23.1728) / 43.6082

            d_p3_lq = (d_p3_lq + 23.1728) / 43.6082
            d_p4_lq = (d_p4_lq + 23.1728) / 43.6082
            d_p5_lq = (d_p5_lq + 23.1728) / 43.6082


            # ----------IRN
            with torch.no_grad():
                for channel_idx in range(d_p2.size()[1]//4):
                    d = d_p2[:, channel_idx*4 : (channel_idx*4+4) ,:,:]
                    d = d.to(device)
                    # interface
                    x_l = IRN_inference(IRN_net, d, device)
                    irn_target_merge[:,channel_idx*4: (channel_idx*4+4),:,:] = x_l[:, :,:,:]
            # ----------
            # codec
            # merge for codec input
            # notice: 4 times of net process, deal with the local sum
            local_sum_bpp = 0
            temp_feat_out = []
            # p2
            out_net = model(irn_target_merge)
            irn_target_merge_lq = out_net["x_hat"]
            out_criterion = criterion(out_net, irn_target_merge, irn_target_merge_lq, None, None, H_in=(int)(jpg_size[0]), W_in=(int)(jpg_size[1]))
            local_sum_bpp += out_criterion['bpp_loss']
            mse_loss.update(out_criterion["mse_loss"])

            # p3
            out_net = model(d_p3)
            d_p3_lq = out_net["x_hat"]
            temp_feat_out.append(_tensor_split(d_p3_lq, p3.shape, channel_factor=4)[0])
            out_criterion = criterion(out_net, d_p3, d_p3_lq, None, None, H_in=(int)(jpg_size[0]), W_in=(int)(jpg_size[1]))
            local_sum_bpp += out_criterion['bpp_loss']
            mse_loss.update(out_criterion["mse_loss"])

            # p4
            out_net = model(d_p4)
            d_p4_lq = out_net["x_hat"]
            temp_feat_out.append(_tensor_split(d_p4_lq, p4.shape, channel_factor=4)[0])
            out_criterion = criterion(out_net, d_p4, d_p4_lq, None, None, H_in=(int)(jpg_size[0]), W_in=(int)(jpg_size[1]))
            local_sum_bpp += out_criterion['bpp_loss']
            mse_loss.update(out_criterion["mse_loss"])

            # p5
            out_net = model(d_p3)
            d_p3_lq = out_net["x_hat"]
            temp_feat_out.append(_tensor_split(d_p5_lq, p5.shape, channel_factor=4)[0])
            out_criterion = criterion(out_net, d_p5, d_p5_lq, None, None, H_in=(int)(jpg_size[0]), W_in=(int)(jpg_size[1]))
            local_sum_bpp += out_criterion['bpp_loss']
            mse_loss.update(out_criterion["mse_loss"])
            # loss backward


            # TODO
            # deal bpp psnr
            bpp_loss.update(local_sum_bpp)



            # split for codec output
            with torch.no_grad():
                for channel_idx in range(d_p2.size()[1]//4):
                    irn_rev_input = irn_target_merge_lq[:, channel_idx*4:(channel_idx+1)*4,:,:]
                    x_hat = IRN_inference(IRN_net, irn_rev_input, device, rev=True)
                    # out_criterion = criterion(None, d, x_hat, x_l, None)
                    irn_res_merge[:,channel_idx*4: (channel_idx*4+4),:,:] = x_hat[:, :,:,:]
            temp_feat_out.insert(0, _tensor_split(irn_res_merge, p2.shape, channel_factor=4)[0])

            # ---------------
            # out_criterion_big = criterion(output=out_net, target = d_p2, lq = irn_res_merge)
            # loss.update(out_criterion['loss'])
            loss_p1 = mse2psnr(out_criterion['mse_loss'])
            # mse_loss.update(out_criterion["mse_loss"])
            # psnr_1.update(loss_p1)
            aux_loss.update(model.aux_loss())

            print("temp_data:mse1:{},psnr1:{}, bpp:{}. bpp_all:{}".format(out_criterion['mse_loss'],loss_p1, out_criterion['bpp_loss'], local_sum_bpp))
            
            
            
            # returen norm
            for p_idx in range(len(temp_feat_out)):
                temp_feat_out[p_idx] = temp_feat_out[p_idx] * 43.6082 - 23.1728
                temp_feat_out[p_idx] = temp_feat_out[p_idx].cpu()
            #     print("{}--shape:{}".format(p_idx, temp_feat_out[p_idx].shape))
            # raise ValueError("success.")
            # ywz done
            save_dir = "./experiments/exp_test/images_after_4irn_256codec/"
            if not os.path.exists(save_dir):
                os.mkdir(save_dir)
                print("not exist save dir:{}, has created.".format(save_dir))
            # save_dir = "./experiments/exp_test/test/"
            # save ywz
            # raise ValueError(temp_path)
            print(temp_path[0].split(".")[-2].split("/")[-1])
            features_png = fea2png(feat=[temp_feat_idx.squeeze() for temp_feat_idx in temp_feat_out])
            save_png(features_png, temp_path[0].split(".")[-2].split("/")[-1], save_dir, "single")
            # done ywz
            # raise ValueError("success.")
            # i = i + 1
            # print(i)

            # tb_logger.add_image('debug_p2_in', (p2[0, 0:1, :, :] + 23.1728) / 43.6082, global_step=i, dataformats='CHW')  # dataformats='HWC')
            # tb_logger.add_image('debug_p2_out', (temp_feat_out[0][0, 0:1, :, :] + 23.1728) / 43.6082, global_step=i, dataformats='CHW')  # dataformats='HWC')
            # raise ValueError("p2_in:{},,----,,p2_out:{}".format(p2[0, 0:1, :, :],temp_feat_out[0][0, 0:1, :, :]))
    tb_logger.add_scalar('{}'.format('[val]: loss'), loss.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: bpp_loss'), bpp_loss.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: psnr'), psnr.avg, epoch + 1)
    tb_logger.add_scalar('{}'.format('[val]: dpsnr'), psnr_2.avg - psnr_1.avg, epoch + 1)
    # logger_val.info(
    print(
        f"Test epoch {epoch}: Average losses: "
        f"Loss: {loss.avg:.4f} | "
        f"MSE loss: {mse_loss.avg:.4f} | "
        f"Bpp loss: {bpp_loss.avg:.4f} | "
        f"Aux loss: {aux_loss.avg:.2f} | "
        # f"PSNR: {psnr.avg:.6f} | "
        # f"MS-SSIM: {ms_ssim.avg:.6f} | "
        f"PSNR_1: {psnr_1.avg:.6f} | "
        # f"PSNR_2: {psnr_2.avg:.6f} | "
        f"BPP_SUM: {bpp_loss.sum:.6f} | "
    )
    return loss.avg


def IRN_inference(model, input, device, rev=False):
    if rev:
        b, c, h, w = input.shape
        z = torch.randn([b, 3 * c, h, w]).to(device)
        out = model(torch.cat((input, z), 1), rev)

    else:
        x_forward = model(input)
        out = x_forward[:, :irn_channels_num, :, :]

    return out


def save_checkpoint(state, is_best, suffix, filename="checkpoint.pth.tar"):
    torch.save(state, filename)
    if is_best:
        dest_filename = filename.replace(filename.split('/')[-1], suffix + "_checkpoint_best_loss.pth.tar")
        shutil.copyfile(filename, dest_filename)


def load_ARCNN(load_path, network, strict=True):
    if isinstance(network, nn.DataParallel):
        network = network.module
    load_net = torch.load(load_path)
    load_net_clean = OrderedDict()  # remove unnecessary 'module.'
    for k, v in load_net['network']['net'].items():
        if k.startswith('module.'):
            load_net_clean[k[7:]] = v
        else:
            load_net_clean[k] = v
    network.load_state_dict(load_net_clean, strict=strict)


def load_IRN(path, network):
    load_path = path
    load_network(load_path, network, True)


def load_network(load_path, network, strict=True):
    if isinstance(network, nn.DataParallel):
        network = network.module
    load_net = torch.load(load_path)

    load_net_clean = OrderedDict()  # remove unnecessary 'module.'
    for k, v in load_net.items():
        if k.startswith('module.'):
            load_net_clean[k[7:]] = v
        else:
            load_net_clean[k] = v
    network.load_state_dict(load_net_clean, strict=strict)


def load_En(checkpoint, net):
    state_dicts = torch.load(checkpoint)
    network_state_dict = {k: v for k, v in state_dicts['net'].items() if 'tmp_var' not in k}
    net.load_state_dict(network_state_dict)


def parse_args(argv):
    parser = argparse.ArgumentParser(description="Example training script.")
    parser.add_argument(
        "-m",
        "--model",
        default="bmshj2018-factorized",
        choices=image_models.keys(),
        help="Model architecture (default: %(default)s)",
    )
    # parser.add_argument(
    #     "-d", "--dataset", type=str, required=True, help="Training dataset"
    # )
    parser.add_argument(
        "-d_test", "--test_dataset", type=str, required=True, help="Training dataset"
    )
    parser.add_argument(
        "-e",
        "--epochs",
        default=5000,
        type=int,
        help="Number of epochs (default: %(default)s)",
    )
    parser.add_argument(
        "-lr",
        "--learning-rate",
        default=1e-4,
        type=float,
        help="Learning rate (default: %(default)s)",
    )
    parser.add_argument(
        "-n",
        "--num-workers",
        type=int,
        default=4,
        help="Dataloaders threads (default: %(default)s)",
    )
    parser.add_argument(
        "-q",
        "--quality",
        type=int,
        default=1,
    )
    parser.add_argument(
        "--lambda",
        dest="lmbda",
        type=float,
        default=1e-2,
        help="Bit-rate distortion parameter (default: %(default)s)",
    )
    parser.add_argument(
        "--batch-size", type=int, default=16, help="Batch size (default: %(default)s)"
    )
    parser.add_argument(
        "--test-batch-size",
        type=int,
        default=1,
        help="Test batch size (default: %(default)s)",
    )
    parser.add_argument(
        "--aux-learning-rate",
        default=1e-3,
        help="Auxiliary loss learning rate (default: %(default)s)",
    )
    parser.add_argument(
        "--patch-size",
        type=int,
        nargs=2,
        default=(256, 256),
        help="Size of the patches to be cropped (default: %(default)s)",
    )
    # parser.add_argument("--cuda", action="store_true", help="Use cuda")
    parser.add_argument(
        "--cuda",
        type=int,
        default=-1,
        help="Use cuda",
    )
    parser.add_argument(
        "--save", action="store_true", default=True, help="Save model to disk"
    )
    parser.add_argument(
        "--seed", type=float, help="Set random seed for reproducibility"
    )
    parser.add_argument(
        "--clip_max_norm",
        default=1.0,
        type=float,
        help="gradient clipping max norm (default: %(default)s",
    )
    parser.add_argument(
        "-exp", "--experiment", type=str, required=True, help="Experiment name"
    )
    parser.add_argument("--irncheckpoint", type=str, help="Path to a irncheckpoint")
    parser.add_argument("--checkpoint", type=str, help="Path to a checkpoint")
    parser.add_argument("--en", type=str, help="Path to the EN checkpoint")
    args = parser.parse_args(argv)
    return args


def main(argv):
    args = parse_args(argv)

    if args.seed is not None:
        torch.manual_seed(args.seed)
        random.seed(args.seed)

    if not os.path.exists(os.path.join('experiments', args.experiment)):
        os.makedirs(os.path.join('experiments', args.experiment))

    print(args.experiment)
    # util.setup_logger('train', os.path.join('experiments', args.experiment), 'train_' + args.experiment,
    #                   level=logging.INFO,
    #                   screen=True, tofile=True)
    util.setup_logger('val', os.path.join('experiments', args.experiment), 'val_' + args.experiment,
                      level=logging.INFO,
                      screen=True, tofile=True)

    logger_train = logging.getLogger('train')
    logger_val = logging.getLogger('val')

    tb_logger = SummaryWriter(log_dir='./tb_logger/' + args.experiment)

    if not os.path.exists(os.path.join('experiments', args.experiment, 'checkpoints')):
        os.makedirs(os.path.join('experiments', args.experiment, 'checkpoints'))
    # train_transforms = transforms.Compose(
    #     [transforms.RandomCrop(args.patch_size), transforms.ToTensor()]
    # )

    test_transforms = transforms.Compose(
        [transforms.ToTensor()]
    )

    # test_dataset = ImageFolder(args.test_dataset, split="", transform=test_transforms)
    test_dataset = FeatureFolder(args.test_dataset, split="", mode="all_with_size", only_little=False)#, transform=test_transforms)

    device = "cuda" if args.cuda!=-1 and torch.cuda.is_available() else "cpu"
    if device == 'cuda':
        print("using cuda")
        torch.cuda.set_device(args.cuda)

    test_dataloader = DataLoader(
        test_dataset,
        batch_size=args.test_batch_size,
        num_workers=args.num_workers,
        shuffle=False,
        pin_memory=("cuda" in device),
    )

    # net = image_models[args.model](quality=args.quality)
    # net = ScaleHyperpriorMulti(M = 192, N = 128)
    net = Cheng2020Anchor256(N = 192)
    net = net.to(device)
    IRN_net = InvRescaleNet(irn_channels_num, irn_channels_num, subnet("DBNet", 'xavier'), [8], 1)
    IRN_net = IRN_net.to(device)
    # En_net = ARCNNModel()
    En_net = EnModule(64, 3, 16, 10, 5)
    En_net = En_net.to(device)

    # if args.cuda and torch.cuda.device_count() > 1:
        # net = CustomDataParallel(net)
        # IRN_net = CustomDataParallel(IRN_net)
        # En_net = CustomDataParallel(En_net)

    logger_train.info(args)
    logger_train.info(net)
    logger_train.info(IRN_net)
    logger_train.info(En_net)

    # Load IRN
    # IRN_path = "./experiments/exp_cheng_En_01_only_q4/checkpoints/IRN_net_checkpoint_best_loss.pth.tar"
    # checkpoint = torch.load(IRN_path, map_location=lambda storage, loc: storage)
    # IRN_net.load_state_dict(checkpoint['state_dict'])

    # Load EN
    # # EN_path = "./experiments/exp_cheng_En_02_only_q3/checkpoints/En_net_checkpoint_best_loss.pth.tar"
    # EN_path = args.en
    # print("Loading", args.en)
    # checkpoint = torch.load(EN_path, map_location=lambda storage, loc: storage)
    # En_net.load_state_dict(checkpoint['state_dict'])
    # # EN_path = "/home/jjp/CompressAI/pretrained_model/ARCNN/ckp_first_best.pt"
    # # load_ARCNN(EN_path, En_net)

    IRN_path = args.irncheckpoint or "/media/data/yangwenzhe/ywzCompressAI/pretrained_model/IRN/x2/IRN_x2.pth"
    try:
        checkpoint = torch.load(IRN_path, map_location=lambda storage, loc: storage)
        IRN_net.load_state_dict(checkpoint['state_dict'])
    except:
        print("load ori IRN")
        load_IRN(IRN_path, IRN_net)
        # temp_epoch(test_dataloader, IRN_net)

    criterion = RateDistortionLoss(lmbda=args.lmbda)

    last_epoch = 0
    if args.checkpoint:  # load from previous checkpoint
        print("Loading", args.checkpoint)
        # checkpoint = torch.load(args.checkpoint, map_location=device)
        # last_epoch = checkpoint["epoch"] + 1
        # net.load_state_dict(checkpoint)
        # net.load_state_dict(checkpoint["state_dict"])
        # optimizer.load_state_dict(checkpoint["optimizer"])
        # aux_optimizer.load_state_dict(checkpoint["aux_optimizer"])
        # lr_scheduler.load_state_dict(checkpoint["lr_scheduler"])

        checkpoint = torch.load(args.checkpoint, map_location=lambda storage, loc: storage)

        # for key in list(checkpoint):
        #     if key.split(".")[0] == "entropy_bottleneck":
        #         if key.split(".")[1] == "_biases":
        #             checkpoint[key.split(".")[0] + '._bias' + key.split(".")[2]] = checkpoint[key]
        #             del (checkpoint[key])
        #         if key.split(".")[1] == "_factors":
        #             checkpoint[key.split(".")[0] + '._factor' + key.split(".")[2]] = checkpoint[key]
        #             del (checkpoint[key])
        #         if key.split(".")[1] == "_matrices":
        #             checkpoint[key.split(".")[0] + '._matrix' + key.split(".")[2]] = checkpoint[key]
        #             del (checkpoint[key])
        # net.load_state_dict(checkpoint)
        # ^
        net.load_state_dict(checkpoint['state_dict'])
        # checkpoint.keys()
        # new_dict = net.state_dict()
        # pretrained_dict = {k: v for k, v in checkpoint.items() if
        #                    (k in new_dict)}  # filter out unnecessary keys
        # new_dict.update(pretrained_dict)
        # net.load_state_dict(new_dict)

        # logger_train.info(f"Learning rate: {optimizer.param_groups[0]['lr']}")

    loss = test_epoch(0, test_dataloader, net, IRN_net, En_net, criterion, logger_val, tb_logger)

if __name__ == "__main__":
    main(sys.argv[1:])
