from bjontegaard_metric import *
# from pandas.tests.extension.numpy_.test_numpy_nested import np
from operator import itemgetter
import math
import xlrd
import sys

item_size = 4 #psnr,bpp,ssim,lpips顺序
y_bias = 0 
if len(sys.argv) > 1:
    y_bias = int(sys.argv[1])
y_name = ['PSNR','BPP-no-use','SSIM','LPIPS'][y_bias]
print(y_name)
# name_index = 0
# database_name = ['Instereo2k', 'KITTI2', 'sPeking'][name_index]


## datasheet 读画
wb = xlrd.open_workbook(r'data.xlsx')
#sheet1索引从0开始，得到sheet1表的句柄
sheet1 = wb.sheet_by_index(0)
rowNum = sheet1.nrows
colNum = sheet1.ncols

#基准
x_mark = []
y_mark = []
for ii in range(colNum//item_size):
    name = sheet1.col_values(ii*item_size)[0]
    # if name.strip() == 'image-anchor': #基准
    if name.strip() == 'p-anchor': #基准
        y_mark = sheet1.col_values(ii*item_size+y_bias) #y-val
        x_mark = sheet1.col_values(ii*item_size+1) #bpp
        while len(x_mark)>1 and x_mark[-1]=='':
            x_mark.pop()
            y_mark.pop()
        #plot
        x_mark, y_mark = zip(*sorted(zip(x_mark[1:], y_mark[1:]), key=itemgetter(0)))

        break

# ###
x1 = []
y1 = []
for ii in range(colNum//item_size):
    name = sheet1.col_values(ii*item_size)[0]
    y1 = sheet1.col_values(ii*item_size+y_bias) #y-val
    x1 = sheet1.col_values(ii*item_size+1) #bpp
    while len(x1)>1 and x1[-1]=='':
        x1.pop()
        y1.pop()

    #plot
    x1, y1 = zip(*sorted(zip(x1[1:], y1[1:]), key=itemgetter(0)))

    print(name)
    print('BD-PSNR: ', BD_PSNR(x_mark, y_mark, x1, y1))
    print('BD-RATE: ', BD_RATE(x_mark, y_mark, x1, y1))

# image-anchor
# x_mark = [0.037, 0.078, 0.153, 0.287, 0.509, 0.863]
# y_mark = [0.58021, 0.68842, 0.73963, 0.77263, 0.77989, 0.78929]
# p-anchor
# x_mark = [0.194, 0.292, 0.437, 0.641, 0.931, 1.355]
# y_mark = [0.5972, 0.6965, 0.7475, 0.7705, 0.7806, 0.7887]

# x1 = [0.023, 0.042, 0.061, 0.077, 0.114, 0.147]
# y1 = [0.71423, 0.75662, 0.77032, 0.77565, 0.77762, 0.78232]

# print("---")
# print('BD-PSNR: ', BD_PSNR(x_mark, y_mark, x1, y1))
# print('BD-RATE: ', BD_RATE(x_mark, y_mark, x1, y1))
