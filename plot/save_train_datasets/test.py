import argparse
import csv
import glob
import json
import os

import utils 
from eval import DetectEval

# os.environ['CUDA_VISIBLE_DEVICES'] = '0'
# os.environ["DETECTRON2_DATASETS"] = '/media/data/minglang/data/m57343_objdet_small_1/'s
os.environ["DETECTRON2_DATASETS"] = '/media/data/liutie/VCM/OpenImageV6-5K'
# os.environ["DETECTRON2_DATASETS"] = "/media/data/liutie/VCM/OpenImage_smaller_30k"


if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-i", "--index", default=37, type=int) #1 
    parser.add_argument("-n", "--number", default=5000, type=int)
    parser.add_argument("-m", "--mode", default='feature_coding')
    parser.add_argument("-a", "--analysis", default='nothing') # analysis on lossy compression of feature
    parser.add_argument("-qp", "--qp_val", default=41) # analysis on lossy compression of feature
    parser.add_argument("-com", "--compress_layer", default='nothing')
    parser.add_argument("-en", "--encoding", action='store_true')
    parser.add_argument("-image", "--image_path", default='./')

    parser.add_argument("--save_path", default='./', type=str) # save featuremap
    parser.add_argument("--compress_path", 
                        default="./",
                        type=str)
    
    # parser.add_argument("--save_path", default='/media/data/minglang/data/feature_compression/set_feature_zero_0527_actual_resize/', type=str)
    # parser.add_argument("--save_path", default='/media/data/minglang/data/feature_compression/set_feature_zero_0531_actual_resize/', type=str)
    parser.add_argument("--save_channel_num_as_zeros", default=0, type=int) # 'down2_qp41_sensibility'
    # parser.add_argument("-m", "--mode", default='evaluation')

    args = parser.parse_args()
    set_idx = args.index
    number = args.number
    mode = args.mode 

    # os.environ["DETECTRON2_DATASETS"] = args.image_path
    # print(">>> os.environ", os.environ["DETECTRON2_DATASETS"])
    # tt

    save_path = args.save_path
    save_channel_num = args.save_channel_num_as_zeros

    with open(f"settings/{set_idx}.json", "r") as setting_json:
    # with open("settings/{set_idx}.json", "r") as setting_json:
        settings = json.load(setting_json)

    # print(">>>> settings: ", settings)
    # tt

    if settings["model_name"] == "x101":
        methods_eval = DetectEval(settings, set_idx, args)
        if mode == "feature_coding_ml_v2":
            picklist = sorted(glob.glob(os.path.join(os.environ["DETECTRON2_DATASETS"], "*.jpg")))[:number]
        else:
            picklist = sorted(glob.glob(os.path.join(os.environ["DETECTRON2_DATASETS"], "*.jpg")))[:]

        picklist = [utils.simple_filename(x) for x in picklist]

        # print(">>>> dir: ", os.environ["DETECTRON2_DATASETS"], len(picklist))
        # tt
        methods_eval.prepare_part(picklist, data_name="pick")

    if mode == "feature_coding":
        filenames = methods_eval.feature_coding()
    elif mode == "feature_coding_ml":
        filenames = methods_eval.feature_coding_ml(save_path, save_channel_num)
    elif mode == "feature_coding_ml_v2":
        filenames = methods_eval.feature_coding_ml_v2(save_path, save_channel_num)

    elif mode == "evaluation":
        # filenames = glob.glob(f"feature/{set_idx}_rec/*.png")
        # filenames = glob.glob(
        #     f"/media/data/liutie/VCM/rcnn/VCM_EE1.2_P-layer_feature_map_anchor_generation_137th_MPEG-VCM-main/feature/{set_idx}_rec/*.png")
        filenames = glob.glob(f"feature/35_ori/*.png")
        
        # filenames = glob.glob(f"/media/data/minglang/m58786/feature/35_rec_liu/*.png")
        # filenames = glob.glob(f"/media/data/minglang/m58786/feature/35_ori_liu/*.png")
        # filenames = glob.glob("feature/{set_idx}_rec/*.png")
        methods_eval.evaluation(filenames) 

    elif mode == "evaluation_ml":
        # print(">>>> save path: ", f"{save_path}/rm_{save_channel_num}_channels/")

        # filenames = glob.glob(f"{save_path}/rm_{save_channel_num}_channels/*.npy")
        # filenames = glob.glob(f"{save_path}/rm_{save_channel_num}_channels/*.png")
        # filenames = glob.glob(f"{args.save_path}/rm_{save_channel_num}_channels/*.png")
        filenames = glob.glob(f"{args.save_path}/*.png")
        print(">>> filenames: ", len(filenames))
        methods_eval.evaluation(filenames, save_channel_num)

    elif mode == "evaluation_offline":
        methods_eval.evaluation_offline()