import os
import subprocess
from tqdm import tqdm
import random
import cv2
import numpy as np
import matplotlib.pyplot as plt

# source_path = "/media/data/liutie/VCM/OpenImageV6-5K/"
# yzw_path = "/media/data/ywz/ywz_vivo/ywzCompressAI/experiments/exp_test/images_quant/" # "/media/data/minglang/m58786/dataset/ywzvivoout/"
# yzw_renamed_path = "/media/data/minglang/m58786/dataset/ywzvivoout_renamed/"
# source_feature_path = "/media/data/minglang/data/feature_compression/set_feature_zero/rm_0_channels/"
# img_list1 = os.listdir(yzw_renamed_path)
# name = img_list1[0]


# print(">>>> source_feature_path + name: ", source_feature_path + name)

# img_raw = cv2.imread(source_feature_path + name, -1).astype(np.float32)
# img_wz = cv2.imread(yzw_renamed_path + name, -1).astype(np.float32)

# plt.subplot(121)
# plt.imshow(img_raw)
# plt.colorbar()
# plt.subplot(122)
# plt.imshow(img_wz)
# plt.colorbar()
# plt.show()

# img_list1 = os.listdir(yzw_path)
# img_list2 = os.listdir(source_path)

# for i_img, name in enumerate(img_list1):
#     imglist2 = [i for i in img_list2 if i[:-8] ==  name[:-4]]
#     print(">>> imglist2: ", imglist2)
#     if len(imglist2) != 1:
#         tt
    
#     read_name = imglist2[0]
#     cmd = "cp {} {}".format(yzw_path + name, yzw_renamed_path + read_name[:-4] +  ".png")
#     subprocess.call([cmd], shell=True)


def cp_train_30k_images():

    train_path = "/media/data/ccr/OIdataset/train/OpenImage_smaller/"
    train_30k_path = "/media/data/minglang/m58786/dataset/OpenImage_smaller_30k/"
    img_list1 = os.listdir(train_path)

    random.shuffle(img_list1)
    print(">>>> img_list1: ", len(img_list1))

    pbar = tqdm(total=len(img_list1[:30000]))
    for i_img, name in enumerate(img_list1[:30000]):
        pbar.update(1)

        cmd = "cp {} {}".format(train_path+name, train_30k_path+name)  
        subprocess.call([cmd], shell=True)

def cp_part_of_test_images():

    img_list1 = os.listdir(yzw_path)
    img_list2 = os.listdir(source_path)



if __name__ == "__main__":
    # cp_part_of_test_images()
    cp_train_30k_images()