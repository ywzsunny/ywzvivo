import glob
import json
import os
import shutil
from datetime import datetime
from tkinter import ttk
from unicodedata import category
import torch.nn as nn
import torch.nn.functional as F

# import imagesize
from detectron2.config import get_cfg
from detectron2.data.datasets import register_coco_instances
from detectron2.engine import DefaultPredictor
import numpy as np
import torch
# import matplotlib.gridspec as gridspec
import matplotlib.pyplot as plt
import copy

categories_ml = [
        {
            "supercategory": "person_ml",
            "id": 0,
            "name": "background"
        },
        {
            "supercategory": "person_ml",
            "id":66,
            "name": "others1"
        },
        {
            "supercategory": "person_ml",
            "id":26,
            "name": "others3"
        },
        {
            "supercategory": "person",
            "id": 1,
            "name": "person"
        },
        {
            "supercategory": "vehicle",
            "id": 2,
            "name": "bicycle"
        },
        {
            "supercategory": "vehicle",
            "id": 3,
            "name": "car"
        },
        {
            "supercategory": "vehicle",
            "id": 4,
            "name": "motorcycle"
        },
        {
            "supercategory": "vehicle",
            "id": 5,
            "name": "airplane"
        },
        {
            "supercategory": "vehicle",
            "id": 6,
            "name": "bus"
        },
        {
            "supercategory": "vehicle",
            "id": 7,
            "name": "train"
        },
        {
            "supercategory": "vehicle",
            "id": 8,
            "name": "truck"
        },
        {
            "supercategory": "vehicle",
            "id": 9,
            "name": "boat"
        },
        {
            "supercategory": "outdoor",
            "id": 10,
            "name": "traffic light"
        },
        {
            "supercategory": "outdoor",
            "id": 11,
            "name": "fire hydrant"
        },
        {
            "supercategory": "outdoor",
            "id": 13,
            "name": "stop sign"
        },
        {
            "supercategory": "outdoor",
            "id": 14,
            "name": "parking meter"
        },
        {
            "supercategory": "outdoor",
            "id": 15,
            "name": "bench"
        },
        {
            "supercategory": "animal",
            "id": 16,
            "name": "bird"
        },
        {
            "supercategory": "animal",
            "id": 17,
            "name": "cat"
        },
        {
            "supercategory": "animal",
            "id": 18,
            "name": "dog"
        },
        {
            "supercategory": "animal",
            "id": 19,
            "name": "horse"
        },
        {
            "supercategory": "animal",
            "id": 20,
            "name": "sheep"
        },
        {
            "supercategory": "animal",
            "id": 21,
            "name": "cow"
        },
        {
            "supercategory": "animal",
            "id": 22,
            "name": "elephant"
        },
        {
            "supercategory": "animal",
            "id": 23,
            "name": "bear"
        },
        {
            "supercategory": "animal",
            "id": 24,
            "name": "zebra"
        },
        {
            "supercategory": "animal",
            "id": 25,
            "name": "giraffe"
        },
        {
            "supercategory": "accessory",
            "id": 27,
            "name": "backpack"
        },
        {
            "supercategory": "accessory",
            "id": 28,
            "name": "umbrella"
        },
        {
            "supercategory": "accessory",
            "id": 31,
            "name": "handbag"
        },
        {
            "supercategory": "accessory",
            "id": 32,
            "name": "tie"
        },
        {
            "supercategory": "accessory",
            "id": 33,
            "name": "suitcase"
        },
        {
            "supercategory": "sports",
            "id": 34,
            "name": "frisbee"
        },
        {
            "supercategory": "sports",
            "id": 35,
            "name": "skis"
        },
        {
            "supercategory": "sports",
            "id": 36,
            "name": "snowboard"
        },
        {
            "supercategory": "sports",
            "id": 37,
            "name": "sports ball"
        },
        {
            "supercategory": "sports",
            "id": 38,
            "name": "kite"
        },
        {
            "supercategory": "sports",
            "id": 39,
            "name": "baseball bat"
        },
        {
            "supercategory": "sports",
            "id": 40,
            "name": "baseball glove"
        },
        {
            "supercategory": "sports",
            "id": 41,
            "name": "skateboard"
        },
        {
            "supercategory": "sports",
            "id": 42,
            "name": "surfboard"
        },
        {
            "supercategory": "sports",
            "id": 43,
            "name": "tennis racket"
        },
        {
            "supercategory": "kitchen",
            "id": 44,
            "name": "bottle"
        },
        {
            "supercategory": "kitchen",
            "id": 45,
            "name": "bottle1"
        },
        {
            "supercategory": "kitchen",
            "id": 46,
            "name": "wine glass"
        },
        {
            "supercategory": "kitchen",
            "id": 47,
            "name": "cup"
        },
        {
            "supercategory": "kitchen",
            "id": 48,
            "name": "fork"
        },
        {
            "supercategory": "kitchen",
            "id": 49,
            "name": "knife"
        },
        {
            "supercategory": "kitchen",
            "id": 50,
            "name": "spoon"
        },
        {
            "supercategory": "kitchen",
            "id": 51,
            "name": "bowl"
        },
        {
            "supercategory": "food",
            "id": 52,
            "name": "banana"
        },
        {
            "supercategory": "food",
            "id": 53,
            "name": "apple"
        },
        {
            "supercategory": "food",
            "id": 54,
            "name": "sandwich"
        },
        {
            "supercategory": "food",
            "id": 55,
            "name": "orange"
        },
        {
            "supercategory": "food",
            "id": 56,
            "name": "broccoli"
        },
        {
            "supercategory": "food",
            "id": 57,
            "name": "carrot"
        },
        {
            "supercategory": "food",
            "id": 58,
            "name": "hot dog"
        },
        {
            "supercategory": "food",
            "id": 59,
            "name": "pizza"
        },
        {
            "supercategory": "food",
            "id": 60,
            "name": "donut"
        },
        {
            "supercategory": "food",
            "id": 61,
            "name": "cake"
        },
        {
            "supercategory": "furniture",
            "id": 62,
            "name": "chair"
        },
        {
            "supercategory": "furniture",
            "id": 63,
            "name": "couch"
        },
        {
            "supercategory": "furniture",
            "id": 64,
            "name": "potted plant"
        },
        {
            "supercategory": "furniture",
            "id": 65,
            "name": "bed"
        },
        {
            "supercategory": "furniture",
            "id": 67,
            "name": "dining table"
        },
        {
            "supercategory": "furniture",
            "id": 68,
            "name": "dining table1"
        },
        {
            "supercategory": "furniture",
            "id": 69,
            "name": "dining table2"
        },
        {
            "supercategory": "furniture",
            "id": 70,
            "name": "toilet"
        },
        {
            "supercategory": "furniture",
            "id": 71,
            "name": "toilet2"
        },
        {
            "supercategory": "electronic",
            "id": 72,
            "name": "tv"
        },
        {
            "supercategory": "electronic",
            "id": 73,
            "name": "laptop"
        },
        {
            "supercategory": "electronic",
            "id": 74,
            "name": "mouse"
        },
        {
            "supercategory": "electronic",
            "id": 75,
            "name": "remote"
        },
        {
            "supercategory": "electronic",
            "id": 76,
            "name": "keyboard"
        },
        {
            "supercategory": "electronic",
            "id": 77,
            "name": "cell phone"
        },
        {
            "supercategory": "appliance",
            "id": 78,
            "name": "microwave"
        },
        {
            "supercategory": "appliance",
            "id": 79,
            "name": "oven"
        },
        {
            "supercategory": "appliance",
            "id": 80,
            "name": "toaster"
        },
        {
            "supercategory": "appliance",
            "id": 81,
            "name": "sink"
        },
        {
            "supercategory": "appliance",
            "id": 82,
            "name": "refrigerator"
        },
        {
            "supercategory": "indoor",
            "id": 84,
            "name": "book"
        },
        {
            "supercategory": "indoor",
            "id": 85,
            "name": "clock"
        },
        {
            "supercategory": "indoor",
            "id": 86,
            "name": "vase"
        },
        {
            "supercategory": "indoor",
            "id": 87,
            "name": "scissors"
        },
        {
            "supercategory": "indoor",
            "id": 88,
            "name": "teddy bear"
        },
        {
            "supercategory": "indoor",
            "id": 89,
            "name": "hair drier"
        },
        {
            "supercategory": "indoor",
            "id": 90,
            "name": "toothbrush"
        }
    ]

def simple_filename(filename_ext):
    filename_base = os.path.basename(filename_ext)
    filename_noext = os.path.splitext(filename_base)[0]
    return filename_noext

def model_loader(settings):
    cfg = get_cfg()
    cfg.merge_from_file(settings["yaml_path"])
    cfg.MODEL.WEIGHTS = settings["pkl_path"]
    # cfg.MODEL.ROI_HEADS.SCORE_THRESH_TEST = 0.5
    model = DefaultPredictor(cfg).model
    model.eval()
    for param in model.parameters():
        param.requires_grad = False
    return model, cfg

def pick_coco_exp(name, targetlist, split='test'):
    # print(">>>> name: ", name)
    if os.path.isdir(name): # ml commented
        shutil.rmtree(name)
    os.makedirs(name, exist_ok=True)

    coco_path = os.environ["DETECTRON2_DATASETS"]
    if split in ['test']:
        # for train set
        anno_path = "./dataset/annotations/instances_OpenImage_v6.json" 
    elif split in ['train']:
        # for train set
        anno_path = "/media/data/minglang/m58786/dataset/annotations_train/openimg_anno.json"   

    file_list = glob.glob(os.path.join(coco_path, "*.jpg"))
    file_list = [x for x in file_list if simple_filename(x) in targetlist]

    # print(">>>> file_list: ", file_list)

    file_name_list = [os.path.basename(x) for x in file_list]
    with open(anno_path, "r") as anno_file:
        coco_json = json.load(anno_file)
    my_json = {}

    if split in ['test']:
        my_json["info"] = coco_json["info"]
        my_json["licenses"] = coco_json["licenses"]
        my_json["images"] = []
        my_json["annotations"] = []
        my_json["categories"] = coco_json["categories"]

        my_json["images"].extend(
            [x for x in coco_json["images"] if x["file_name"] in file_name_list]
        )
        image_id_list = [x["id"] for x in my_json["images"]]
        my_json["annotations"].extend(
            [x for x in coco_json["annotations"] if x["image_id"] in image_id_list]
        )
    elif split in ['train']:
        my_json["info"] = 'train'
        my_json["licenses"] = 'train'
        my_json["images"] = []
        my_json["annotations"] = []
        my_json["categories"] = categories_ml
        temp_annotation = []

        my_json["images"].extend(
            [x for x in coco_json if x["file_name"].split("/")[-1] in file_name_list]
        )
        for img_data in my_json["images"]:
            img_path = img_data["file_name"].split("/")[-1]
            img_data["file_name"] = img_path

        for i_data in my_json["images"]:
            id_val = i_data["image_id"]
            i_data['id'] = id_val

        image_id_list = [x["image_id"] for x in my_json["images"]]

        all_img_ann = []
        num = 1
        for x_ann in my_json["images"]:
            id_im = x_ann["image_id"]
            anns = x_ann["annotations"]

            if id_im in image_id_list:
                
                one_img_ann = []
                for i_ann in anns:
                    i_ann["id"] = num
                    i_ann["image_id"] = id_im
                    one_img_ann.append(i_ann)

                    num += 1
                all_img_ann.extend(one_img_ann)

        my_json["annotations"].extend(all_img_ann
        )
        

    for filepath in file_list:
        shutil.copy(filepath, name)

    # print(">>>> my_jsonL: ", my_json)
    # tt
    with open(f"{name}/my_anno.json", "w") as my_file:
        my_file.write(json.dumps(my_json))
    
    register_coco_instances(name, {}, f"{name}/my_anno.json", name)

def print_settings(settings, index):
    model_name = settings["model_name"]
    VTM_param = settings["VTM"]
    print()
    print("Evaluation of proposed methods for:", model_name.upper())
    print(f"Settings ID: {index}")
    print(f"VTM paramerters      : {VTM_param}")


import cv2
import numpy as np

def save_feature_map(filename, features, data_type='np.uint16', save_channel_num=256):
    features_draw = features.copy()
    del features_draw["p6"]
    _save_feature_map(filename, features_draw, data_type=data_type, save_channel_num=save_channel_num)

def compute_min_val_index(list_val):
    min_number = []
    min_index = []
    
    t = copy.deepcopy(list_val)
    for _ in range(len(list_val)):
        number = min(t)
        index = t.index(number)
        t[index] = 10000000000
        min_number.append(number)
        min_index.append(index)

    return min_index, min_number

def cal_varience(fetures):
    fetures = fetures.cpu().numpy()
    variences = [np.var(i) for i in fetures]
    
    return variences


def set_zeros(features, save_channel_num):
    variences = cal_varience(features)
    min_index, min_val = compute_min_val_index(variences)
    zeros_index = min_index[:save_channel_num]
    for i in zeros_index:
        features[i, :, :] = 0

    return features

def resize_feature(features, save_channel_num, size=None):
    chan, h_out1, w_out1 = features.size()
    features_resize = copy.deepcopy(features)
    #
    if save_channel_num == 111:
        pool = nn.MaxPool2d(2, stride=2, return_indices=True)
        unpool = nn.MaxUnpool2d(2, stride=2)
        output, indices = pool(features_resize)
        features_resize = unpool(output, indices)
    elif save_channel_num in [112, 113, 114, 115, 116, 117]:
        # max_val = torch.max(features_resize)
        # min_val = torch.min(features_resize)
        # print(">>>> h_out1, w_out1: ", chan, h_out1, w_out1)

        features_resize = torch.unsqueeze(features_resize, 0)
        features_resize = F.interpolate(features_resize, scale_factor=(0.5,0.5), mode="bilinear", align_corners=False)
        """
        import torch.nn.functional as F
        features_resize = torch.unsqueeze(features_resize, 0)
        features_resize = F.interpolate(features_resize, scale_factor=(0.5,0.5), mode="bilinear", align_corners=False)
        features_resize = F.interpolate(features_resize, scale_factor=(2,2), mode="bilinear", align_corners=False)
        features_resize = torch.squeeze(features_resize, 0)
        """
        features_resize = F.interpolate(features_resize, size=(h_out1, w_out1), mode="bilinear", align_corners=False)
        # print(">>>> h_out1, w_out1: ", features_resize.size())

        features_resize = torch.squeeze(features_resize, 0)
    elif save_channel_num in [212, 213, 214, 215, 216, 217]:
        # features_resize = torch.unsqueeze(features_resize, 0)
        features_resize = features_resize.cpu().numpy() # (c, h, w)
        # print(">>>> h_out1, w_out1: ", np.shape(features_resize))
        features_resize = features_resize.transpose((1,2,0)) # (h, w, c)
        # print(">>>> h_out1, w_out1: ", np.shape(features_resize))
        features_resize = cv2.resize(features_resize, (int(w_out1/2), int(h_out1/2)), interpolation=cv2.INTER_CUBIC)
        # print(">>>> h_out1, w_out1: ", np.shape(features_resize))
        features_resize = cv2.resize(features_resize, (w_out1, h_out1), interpolation=cv2.INTER_CUBIC)
        # print(">>>> h_out1, w_out1: ", np.shape(features_resize))
        features_resize = features_resize.transpose((2,0,1))
        features_resize = torch.from_numpy(features_resize).cuda()
        # print(">>>> h_out1, w_out1: ", features_resize.size())
        # tt
    elif save_channel_num in ['upsample_only']: # upsample only
        h_out_ori, w_out_ori = size
        features_resize = features_resize.cpu().numpy()
        features_resize = features_resize.transpose((1,2,0)) # (chw) to (hwc)
        features_resize = cv2.resize(features_resize, (w_out_ori, h_out_ori), interpolation=cv2.INTER_CUBIC)
        features_resize = features_resize.transpose((2,0,1)) # (hwc) to chw
        features_resize = torch.from_numpy(features_resize).cuda()

    # a = 1
    # tt
    return features_resize

def _save_feature_map(filename, features, debug=False, data_type='np.uint16', save_channel_num=256):
    feat = [features["p2"].squeeze(), features["p3"].squeeze(), features["p4"].squeeze(), features["p5"].squeeze()]

    if not data_type in ['np.uint16'] and save_channel_num >= 1:
        if save_channel_num in [111, 112, 212]:
            features_zeroed = resize_feature(feat[0], save_channel_num)
            feat[0] = features_zeroed
        elif save_channel_num in [113, 213]: # do bilinear for P3
            features_zeroed = resize_feature(feat[1], save_channel_num)
            feat[1] = features_zeroed
        elif save_channel_num in [114, 214]: # do bilinear for P4
            features_zeroed = resize_feature(feat[2], save_channel_num)
            feat[2] = features_zeroed
        elif save_channel_num in [115, 215]: # do bilinear for P5
            features_zeroed = resize_feature(feat[3], save_channel_num)
            feat[3] = features_zeroed
        elif save_channel_num in [116, 117, 216, 217]: # do bilinear for all player
            features_zeroed_p2 = resize_feature(feat[0], save_channel_num)
            features_zeroed_p3 = resize_feature(feat[1], save_channel_num)
            features_zeroed_p4 = resize_feature(feat[2], save_channel_num)
            features_zeroed_p5 = resize_feature(feat[3], save_channel_num)

            feat[0] = features_zeroed_p2
            feat[1] = features_zeroed_p3
            feat[2] = features_zeroed_p4
            feat[3] = features_zeroed_p5
        else:
            pass
            # features_zeroed = set_zeros(feat[0], save_channel_num)
            # feat[0] = features_zeroed

        # features_zeroed = feat[0]
        # for i in range(256):
        #     feat[1][i, :, :] = 0
        #     feat[2][i, :, :] = 0
        #     feat[3][i, :, :] = 0

    width_list = [16, 32, 64, 128]
    height_list = [16, 8, 4, 2]
    tile_big = np.empty((0, feat[0].shape[2] * width_list[0]))
    for blk, width, height in zip(feat, width_list, height_list):
        big_blk = np.empty((0, blk.shape[2] * width))
        for row in range(height):
            big_blk_col = np.empty((blk.shape[1], 0))
            for col in range(width):
                tile = blk[col + row * width].cpu().numpy()
                if debug:
                    cv2.putText(
                        tile,
                        f"{col + row * width}",
                        (32, 32),
                        cv2.FONT_HERSHEY_SIMPLEX,
                        0.5,
                        (255, 255, 255),
                        1,
                    )
                big_blk_col = np.hstack((big_blk_col, tile))
            big_blk = np.vstack((big_blk, big_blk_col))
        tile_big = np.vstack((tile_big, big_blk))
    
    if data_type in ['np.uint16']:
        tile_big = tile_big.astype(np.uint16)
        cv2.imwrite(filename+'.png', tile_big)
    else:
        # print(">>>> filename: ", filename)
        # sub_path = filename[:-4]
        # if not os.path.exists(sub_path):
        #     os.makedirs(sub_path)
        
        # # p2
        # filename_p2 = sub_path + "/p2.npy"
        # filename_p3 = sub_path + "/p3.npy"
        # filename_p4 = sub_path + "/p4.npy"
        # filename_p5 = sub_path + "/p5.npy"

        # np.save(filename_p2, features["p2"].cpu().numpy())
        # np.save(filename_p3, features["p3"].cpu().numpy())
        # np.save(filename_p4, features["p4"].cpu().numpy())
        # np.save(filename_p5, features["p5"].cpu().numpy())

        tile_big = tile_big.astype(np.float32)

        # plt.imshow(tile_big)
        # plt.show()
        # plt.savefig('t1_raw.jpg')
        # tt

        np.save(filename+'.npy', tile_big)

def result_in_list(settings, number, result, set_index):
    res = list(result.values())[0]
    ap = res["AP"]
    ap50 = res["AP50"]
    aps = res["APs"]
    apm = res["APm"]
    apl = res["APl"]

    return [
        datetime.now(),
        set_index,
        number,
        f"{ap:.3f}",
        f"{ap50:.3f}",
        f"{aps:.3f}",
        f"{apm:.3f}",
        f"{apl:.3f}",
    ]

def get_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size
