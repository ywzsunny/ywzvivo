# Copyright (c) 2021-2022, InterDigital Communications, Inc
# All rights reserved.

# Redistribution and use in source and binary forms, with or without
# modification, are permitted (subject to the limitations in the disclaimer
# below) provided that the following conditions are met:

# * Redistributions of source code must retain the above copyright notice,
#   this list of conditions and the following disclaimer.
# * Redistributions in binary form must reproduce the above copyright notice,
#   this list of conditions and the following disclaimer in the documentation
#   and/or other materials provided with the distribution.
# * Neither the name of InterDigital Communications, Inc nor the names of its
#   contributors may be used to endorse or promote products derived from this
#   software without specific prior written permission.

# NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY
# THIS LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND
# CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT
# NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A
# PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
# OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY,
# WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR
# OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF
# ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

from pathlib import Path
import random

from PIL import Image
from torch.utils.data import Dataset
from .parse_p2_feature import *
from .parse_p2_feature import _joint_split_features
# from .utils import * #合图降通道



def padding_size(ori_size, factor_size):
    if ori_size % factor_size == 0:
        return ori_size
    else:
        return factor_size * (ori_size // factor_size + 1)


def _tensor_joint(temp_feat, channel_factor = 3, pixel_factor = 128):
    # temp_feat = [p2,p3,p4,p5]
    ch_factor_res = 0
    for ii in range(len(temp_feat)):
        ch_factor_res += 1/(4**ii)
    out_channels = (int)(ch_factor_res*temp_feat[0].shape[1]) #长宽都是2倍2倍降
    temp_tensor = torch.zeros(temp_feat[0].shape[0], padding_size(out_channels, channel_factor), padding_size(temp_feat[0].shape[2], pixel_factor), padding_size(temp_feat[0].shape[3], pixel_factor))
    channel_start = 0
    for ii in range(len(temp_feat)):
        size_num = 2**ii
        delta_channel_num = temp_feat[0].shape[1] // (size_num * size_num)
        channel_end = channel_start + delta_channel_num
        little_row = temp_feat[ii].shape[2]
        little_col = temp_feat[ii].shape[3]
        channel_idx = 0
        for row in range(size_num):
            for col in range(size_num):
                temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)] = \
                    temp_feat[ii][:, channel_idx:(channel_idx + delta_channel_num),:,:]
                channel_idx += delta_channel_num
        channel_start = channel_end
    return temp_tensor

def _tensor_split(temp_tensor, ori_feat0_shape, channel_factor = 3):
    temp_feat = []
    len_temp_feat = 0
    temp_channels = temp_tensor.shape[1]
    cnt_channels = ori_feat0_shape[1]
    # print("ori channels:{}".format(cnt_channels))
    while temp_channels >= padding_size(cnt_channels, channel_factor):
        len_temp_feat += 1
        if (temp_channels == padding_size(cnt_channels, channel_factor)):
            break
        cnt_channels += ori_feat0_shape[1] * (1/(4**len_temp_feat))
    ##
    # print("len feat:{}".format(len_temp_feat))
    channel_start = 0
    for ii in range(len_temp_feat):
        size_num = 2**ii
        little_row = ori_feat0_shape[2] // size_num
        little_col = ori_feat0_shape[3] // size_num
        temp_rev_tensor = torch.zeros(ori_feat0_shape[0], ori_feat0_shape[1], little_row, little_col)
        delta_channel_num = ori_feat0_shape[1] // (size_num * size_num)
        channel_end = channel_start + delta_channel_num
        channel_idx = 0
        ##
        for row in range(size_num):
            for col in range(size_num):
                temp_rev_tensor[:, channel_idx:(channel_idx + delta_channel_num),:,:] = \
                    temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)]
                channel_idx += delta_channel_num
        channel_start = channel_end
        ##
        temp_feat.append(temp_rev_tensor)
    return temp_feat



# def _tensor_joint(temp_feat, channel_factor = 3):
#     # temp_feat = [p2,p3,p4,p5]
#     out_channels = (int)((1+1/4+1/16+1/64)*temp_feat[0].shape[1]) #长宽都是2倍2倍降
#     temp_tensor = torch.zeros(temp_feat[0].shape[0], padding_size(out_channels, channel_factor), padding_size(temp_feat[0].shape[2], 128), padding_size(temp_feat[0].shape[3], 128))
#     channel_start = 0
#     for ii in range(len(temp_feat)):
#         size_num = 2**ii
#         delta_channel_num = temp_feat[0].shape[1] // (size_num * size_num)
#         channel_end = channel_start + delta_channel_num
#         little_row = temp_feat[ii].shape[2]
#         little_col = temp_feat[ii].shape[3]
#         channel_idx = 0
#         for row in range(size_num):
#             for col in range(size_num):
#                 temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)] = \
#                     temp_feat[ii][:, channel_idx:(channel_idx + delta_channel_num),:,:]
#                 channel_idx += delta_channel_num
#         channel_start = channel_end
#     return temp_tensor

# def _tensor_split(temp_tensor, ori_feat0_shape, channel_factor = 3):
#     temp_feat = []
#     len_temp_feat = 0
#     temp_channels = temp_tensor.shape[1]
#     cnt_channels = ori_feat0_shape[1]
#     # print("ori channels:{}".format(cnt_channels))
#     while temp_channels >= padding_size(cnt_channels, channel_factor):
#         len_temp_feat += 1
#         if (temp_channels == padding_size(cnt_channels, channel_factor)):
#             break
#         cnt_channels += ori_feat0_shape[1] * (1/(4**len_temp_feat))
#     ##
#     # print("len feat:{}".format(len_temp_feat))
#     channel_start = 0
#     for ii in range(len_temp_feat):
#         size_num = 2**ii
#         little_row = ori_feat0_shape[2] // size_num
#         little_col = ori_feat0_shape[3] // size_num
#         temp_rev_tensor = torch.zeros(ori_feat0_shape[0], ori_feat0_shape[1], little_row, little_col)
#         delta_channel_num = ori_feat0_shape[1] // (size_num * size_num)
#         channel_end = channel_start + delta_channel_num
#         channel_idx = 0
#         ##
#         for row in range(size_num):
#             for col in range(size_num):
#                 temp_rev_tensor[:, channel_idx:(channel_idx + delta_channel_num),:,:] = \
#                     temp_tensor[:,channel_start:channel_end,(row*little_row):((row+1)*little_row),(col*little_col):((col+1)*little_col)]
#                 channel_idx += delta_channel_num
#         channel_start = channel_end
#         ##
#         temp_feat.append(temp_rev_tensor)
#     return temp_feat


class FeatureFolder(Dataset):
    """Load an image folder database. Training and testing image samples
    are respectively stored in separate directories:

    .. code-block::

        - rootdir/
            - train/
                - img000.png
                - img001.png
            - test/
                - img000.png
                - img001.png

    Args:
        root (string): root directory of the dataset
        transform (callable, optional): a function or transform that takes in a
            PIL image and returns a transformed version
        split (string): split mode ('train' or 'val')
    """

    def __init__(self, root, transform=None, split="train", mode="train", rate = 1.0, only_little = False):
        splitdir = Path(root) / split

        if not splitdir.is_dir():
            raise RuntimeError(f'Invalid directory "{root}"')

        self.samples = [f for f in splitdir.iterdir() if f.is_file()]
        if only_little:
            self.samples = self.samples[0:8] #[0:(int)(len(self.samples)*rate + 1)]
        else:
            self.samples = self.samples[0:(int)(len(self.samples)*rate + 1)]

        self.transform = transform

        self.mode = mode
        self.ori_jpg_path = "/media/data/liutie/VCM/OpenImageV6-5K/"

    def __getitem__(self, index):
        """
        Args:
            index (int): Index

        Returns:
            img: `PIL.Image.Image` or transformed `PIL.Image.Image`. - to feature
        """
        read_path = self.samples[index]
        # direct from npy
        if self.mode == 'from_npy':
            data = np.load(str(read_path))
            data = torch.from_numpy(np.array(data))
            data = torch.squeeze(data, 0)
            return data, str(read_path)
        # end
        features = feat2feat(read_path)
        p2_feature = features["p2"]
        p3_feature = features["p3"]
        p4_feature = features["p4"]
        p5_feature = features["p5"]
        p2_ori_size = p2_feature.shape
        # print(">>>> p2_feature: ", p2_feature.size())
        # add for img shape bigger
        if self.mode == "train":
            p2_feature = torch.squeeze(p2_feature, 0)
            if p2_feature.shape[1] < 128:
                rand_x_start = 0
                rand_x_end = p2_feature.shape[1]
                print("error in feature:ori_p2_feature:{}".format(p2_feature.shape))
            else:
                rand_x_start = (int)(random.random()*(p2_feature.shape[1] - 128))
                rand_x_end = rand_x_start + 128
            #
            if p2_feature.shape[2] < 128:
                rand_y_start = 0
                rand_y_end = p2_feature.shape[2]
                print("error in feature:ori_p2_feature:{}".format(p2_feature.shape))
            else:
                rand_y_start = (int)(random.random()*(p2_feature.shape[2] - 128))
                rand_y_end = rand_y_start + 128
            #
            p2_feature_new = torch.zeros([p2_feature.shape[0], 128, 128])
            p2_feature_new[:, 0:(rand_x_end - rand_x_start), 0:(rand_y_end - rand_y_start)] = p2_feature[:, rand_x_start:rand_x_end, rand_y_start:rand_y_end]
            p2_feature = p2_feature_new
            # p3_feature = torch.squeeze(p3_feature, 0)
            # p4_feature = torch.squeeze(p4_feature, 0)
            # p5_feature = torch.squeeze(p5_feature, 0)
            return p2_feature,str(read_path)#,p3_feature,p4_feature,p5_feature,str(read_path)
        elif self.mode == "train_all":
            #拼
            merge_tensor = _tensor_joint([p2_feature, p3_feature, p4_feature, p5_feature], channel_factor=4)
            merge_tensor = torch.squeeze(merge_tensor, 0)
            rand_x_start = (int)(random.random()*(p2_ori_size[-2] - 128)) if p2_ori_size[-2] > 128 else 0
            rand_y_start = (int)(random.random()*(p2_ori_size[-1] - 128)) if p2_ori_size[-1] > 128 else 0
            return merge_tensor[:,rand_x_start:(rand_x_start+128),rand_y_start:(rand_y_start+128)], str(read_path)

        elif self.mode == "test" or self.mode == "all":
            #test
            p2_feature = torch.squeeze(p2_feature, 0)
            p3_feature = torch.squeeze(p3_feature, 0)
            p4_feature = torch.squeeze(p4_feature, 0)
            p5_feature = torch.squeeze(p5_feature, 0)
            return p2_feature,p3_feature,p4_feature,p5_feature,str(read_path)
        
        elif self.mode == "padding128":
            # 不拼 RESIZE padding for multi batchsize
            p2_feature = _tensor_joint([p2_feature], channel_factor=4, pixel_factor = 256)[:, :,0:256:,0:256]
            p3_feature = _tensor_joint([p3_feature], channel_factor=4, pixel_factor = 256)[:, :,0:256:,0:256]
            p4_feature = _tensor_joint([p4_feature], channel_factor=4, pixel_factor = 256)[:, :,0:256:,0:256]
            p5_feature = _tensor_joint([p5_feature], channel_factor=4, pixel_factor = 256)[:, :,0:256:,0:256]

            p2_feature = torch.squeeze(p2_feature, 0)
            p3_feature = torch.squeeze(p3_feature, 0)
            p4_feature = torch.squeeze(p4_feature, 0)
            p5_feature = torch.squeeze(p5_feature, 0)
            return p2_feature,p3_feature,p4_feature,p5_feature,str(read_path)
            

        elif self.mode == "all_with_size":
            # add for jpg size
            file_name = str(read_path).split(".")[-2].split("/")[-1]
            jpg_file = os.path.join(self.ori_jpg_path, file_name + ".jpg")
            if not os.path.exists(jpg_file):
                raise ValueError("not exist:{}".format(jpg_file))
            jpg = cv2.imread(jpg_file, -1).astype(np.float32)
            jpg_size = jpg.shape #(683,1024,3)
            #
            p2_feature = torch.squeeze(p2_feature, 0)
            p3_feature = torch.squeeze(p3_feature, 0)
            p4_feature = torch.squeeze(p4_feature, 0)
            p5_feature = torch.squeeze(p5_feature, 0)
            return p2_feature,p3_feature,p4_feature,p5_feature,str(read_path), jpg_size


        # # 按通道拼
        # # add for split merge
        # p2_feature, mode = _joint_split_features(p2_feature, out_channels = 3)
        # p2_feature = torch.from_numpy(p2_feature)
        # # add by ywz
        # p2_feature = torch.squeeze(p2_feature, 0)
        # return p2_feature


    def __len__(self):
        return len(self.samples)

