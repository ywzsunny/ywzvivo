
# vivo云桌面管理服务器文件上传/下载
开发了一套带鉴权的nginx管理网站，账号admin，密码a
```
vivo服务器
下载文件：
1.浏览器输入172.16.35.61:81，点开files
2.在命令行（不要进docker里面），把文件放到/data01/docker_ngmax/files/，然后在1中的网页就能看到 点击即可下载 （尽量打成压缩包更好下载）。

上传文件：
1.在自己电脑打开北航云盘上传文件。
2.在citrix打开云桌面，打开浏览器打开北航网盘，下载文件到桌面上，然后在172.16.35.61:81点mvindex.html 上传文件，之后在vivo服务器命令行进入/data01/docker_ngmax/files/即可看到文件就有了。
```
# vivo共用账号密码
内网gitlab的账号root密码buaamc2vivo 上传nginx网站的账号admin 密码a  三台内网机器互相ssh/scp也开了，账号root密码r

# vivo环境相关
https://vchat.vivo.xyz:8443/vivo/down/pc/index.html#/

72162669
ywz6.62607

https://vdi.vivo.xyz
打开网页内置的谷歌浏览器
https://jus.vivo.xyz/

172.16.35.60
172.16.35.61
172.16.35.62

```
docker commit 容器 镜像
docker save 镜像名[:标签] | gzip > <myimage>.tar.gz
gunzip -c vivo_with_ywz_compressai.tar.gz | docker load

#如果需要拆分-北航云盘同步有大小限制，可以参考如下切分合并
split -b 1024m mydocker.tar.gz "prefixxx."
cat prefixxx* > mydocker.tar.gz

#上传
python setup.py install #bhpan https://github.com/xdedss/dist_bhpan
# bhpan upload [本地文件/文件夹] [远程文件夹(必须不存在,home是自己的根目录)]
bhpan upload -r vivo_with_ywz_compressai_docker home/share/vivo/vivo_docker2

#vivo侧
/usr/local/bin/docker-compose #https://mirrors.aliyun.com/docker-toolbox/linux/compose/1.21.2/docker-compose-Linux-x86_64

#普通文件夹打包
tar -cvvzf ywzvivo.tar.gz ywzvivo
mkdir ywzvivofile
split -b 1024m ywzvivo.tar.gz "ywzvivofile/ywzvivofile."
cat ywzvivofile/ywzvivofile.* > ywzvivo.tar.gz
tar -xvvzf ywzvivo.tar.gz


#vivo侧docker-compose映射
/data01/ywz/ywzvivo
/data01/ccr/media/data/ccr   /OIdataset, model_final.pth, VCM...
```


# 核心文件
```
examples/train.py
compressai/datasets/feature.py
```

# 数据集位置
30901: 
/media/data/minglang/data/feature_compression/set_feature_zero/rm_0_channels
/media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori

# 测试脚本
30901:
python3 examples/train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/ -d_test /media/data/minglang/data/feature_compression/set_feature_zero/rm_0_channels/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001 --cuda 0

使用600+的迷你集：
python3 examples/train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/ywz_create_little_ori/ -d_test /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/ywz_create_little_val/ -q 4 --lambda 0.001 --batch-size 30 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001 --cuda 0

python3 examples/train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/ywz_create_little_ori/ -d_test /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/ywz_create_little_val/ -q 4 --lambda 0.001 --batch-size 30 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001 --cuda 0

/media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001/checkpoints/net_checkpoint_best_loss.pth.tar
/media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001/checkpoints/IRN_net_checkpoint_best_loss.pth.tar

ft:
ft_from_原模型:
python3 examples/train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/ywz_create_little_ori/ -d_test /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/ywz_create_little_val/ -q 4 --lambda 0.001 --batch-size 30 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001 --checkpoint /media/data/yangwenzhe/ywzCompressAI/pretrained_model/bmshj2018-hyperprior/q4/bmshj2018-hyperprior-4-de1b779c.pth.tar --cuda 0

--froze_irn  --froze_codec 支持冻结训练

python3 examples/train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/ -d_test /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/ywz_create_little_val/ -q 4 --lambda 0.001 --batch-size 30 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001 --cuda 0 --checkpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001/checkpoints/net_checkpoint_best_loss.pth.tar --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001/checkpoints/IRN_net_checkpoint_best_loss.pth.tar

冻结训练 --froze_irn 或 --froze_codec , loss:  --mode only_codec only_irn 或不填
python3 examples/new_train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/ -d_test /media/data/minglang/data/feature_compression/set_feature_zero/test_little/ -q 4 --lambda 0.001 --batch-size 25 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001 --cuda 1 --rate 0.1 --froze_irn --mode only_codec

python3 examples/new_train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/ -d_test /media/data/minglang/data/feature_compression/set_feature_zero/test_little/ -q 4 --lambda 0.001 --batch-size 25 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001 --cuda 1 --rate 0.1 --froze_codec --mode only_irn

验证/测试小集合: /media/data/minglang/data/feature_compression/set_feature_zero/test_little/
cd /media/data/minglang/data/feature_compression/set_feature_zero/rm_0_channels/
ls |head -n 100 |xargs -i cp {} ../test_little/
ls -l | grep "^-" | wc -l

测试
python3 examples/validation.py -m bmshj2018-hyperprior -d /media/data/minglang/data/feature_compression/set_feature_zero/rm_0_channels/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save --exp validation_ywz --checkpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001/checkpoints/net_checkpoint_best_loss.pth.tar --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001/checkpoints/IRN_net_checkpoint_best_loss.pth.tar --cuda 1

/media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001/checkpoints/net_checkpoint_best_loss.pth.tar
/media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001/checkpoints/IRN_net_checkpoint_best_loss.pth.tar
/media/data/yangwenzhe/ywzCompressAI/pretrained_model/IRN/x2/IRN_x2.pth

ywz_create_little_val 00007902a7b96e8c.png  0c24141ea5157980.png  1c4cc89654a55a9e.png

#validation 原模型  
python3 examples/validation_offi_all.py -m bmshj2018-hyperprior -d /media/data/minglang/data/feature_compression/set_feature_zero/rm_0_channels/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save --exp exp_ywz_balle_En_03_only_q4 --checkpoint /media/data/yangwenzhe/ywzCompressAI/pretrained_model/bmshj2018-hyperprior/q4/bmshj2018-hyperprior-4-de1b779c.pth.tar --cuda 1


# vivo服务器：
~/data01/minglang
训练，可加--rate 0.1 随机比例
/media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/
测试5000张
/media/data/minglang/m58786/feature/raw_feature/rm_0_channels/35_ori/
测试选前100张  ls |head -n 100 |xargs -i cp {} ../test_little/  ;  ls -l | grep "^-" | wc -l
/media/data/minglang/m58786/feature/raw_feature/rm_0_channels/test_little/

#ft 新模型
--checkpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001_codec/checkpoints/net_checkpoint_best_loss.pth.tar
--irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001_irn/checkpoints/IRN_net_checkpoint_best_loss.pth.tar
#IRN-P2only模型：/media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_irn_p2/checkpoints/IRN_net_checkpoint_best_loss.pth.tar
#IRN过完保存的8张图训练集： /media/data/yangwenzhe/ywzCompressAI/experiments/only_irn_npy_split_4/

CUDA_VISIBLE_DEVICES=1 python3 examples/f0801_new_train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/ -d_test /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/test_little/ -q 4 --lambda 0.001 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001_irn --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001_irn/checkpoints/IRN_net_checkpoint_best_loss.pth.tar --cuda 0 --rate 0.1 --froze_codec --mode only_irn --batch-size 25

CUDA_VISIBLE_DEVICES=0 python3 examples/f0801_new_train.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/ -d_test /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/test_little/ -q 4 --lambda 0.001 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_0.001_codec --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001_irn/checkpoints/IRN_net_checkpoint_best_loss.pth.tar --cuda 0 --rate 0.1 --froze_irn --mode codec_with_irn --batch-size 3
CUDA_VISIBLE_DEVICES=0 python3 examples/f0812_new_train_256.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/ -d_test /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/test_little/ -q 4 --lambda 10.0 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_1e1_256_cheng --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001_irn/checkpoints/IRN_net_checkpoint_best_loss.pth.tar --cuda 0 --rate 0.1 --batch-size 1 --checkpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_1e1_256_cheng/checkpoints/net_checkpoint_best_loss.pth.tar
CUDA_VISIBLE_DEVICES=0 python3 examples/f0901_new_train_256.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/train_png_30k/rm_0_channels/35_ori/ -d_test /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/test_little/ -q 4 --lambda 1.0 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4_1e0_256_cheng --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_0.001_irn/checkpoints/IRN_net_checkpoint_best_loss.pth.tar --cuda 0 --rate 0.1 --batch-size 1 --checkpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_1e0_256_cheng/checkpoints/net_checkpoint_best_loss.pth.tar



#validation 原模型  
p2~p5拼接版
python3 examples/validation_offi_all.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/35_ori/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save --exp exp_ywz_balle_En_03_only_q4 --checkpoint /media/data/yangwenzhe/ywzCompressAI/pretrained_model/bmshj2018-hyperprior/q4/bmshj2018-hyperprior-4-de1b779c.pth.tar --cuda 1

python3 examples/validation_offi_all.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/35_ori/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save --exp exp_ywz_balle_En_03_only_q4 --cuda 1

#测试IRN的db
python3 examples/f0808_validation.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/35_ori/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save --exp exp_ywz_balle_En_03_only_q4 --cuda 1 --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_irn_p2/checkpoints/IRN_net_checkpoint_best_loss.pth.tar --checkpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_1e0_592_cheng/checkpoints/net_checkpoint_best_loss.pth.tar


python3 examples/f0812_validation_256.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/35_ori/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save --exp exp_ywz_balle_En_03_only_q4 --cuda 1 --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_irn_p2/checkpoints/IRN_net_checkpoint_best_loss.pth.tar --checkpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_1e1_256_cheng/checkpoints/net_checkpoint_best_loss.pth.tar
python3 examples/f0901_validation_256.py -m bmshj2018-hyperprior -d /media/data/minglang/m58786/feature/raw_feature/rm_0_channels/35_ori/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save --exp exp_ywz_balle_En_03_only_q4 --cuda 0 --irncheckpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_irn_p2/checkpoints/IRN_net_checkpoint_best_loss.pth.tar --checkpoint /media/data/yangwenzhe/ywzCompressAI/experiments/exp_ywz_balle_En_03_only_q4_1e0_256_cheng/checkpoints/net_checkpoint_best_loss.pth.tar


#仅跑验证 --onlytest

#IRN测试图片类型
python3 examples/f0801_validation_img.py -m bmshj2018-hyperprior -d /media/data/liutie/VCM/OpenImageV6-5K/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save --exp exp_ywz_balle_En_03_only_q4 --cuda 1


# VCM

#train_vcm_ywz.py
python3 train_vcm_ywz.py -m bmshj2018-hyperprior -d /media/data/yangwenzhe/rm_0_channels/ -d_test /media/data/yangwenzhe/rm_0_channels/ -q 4 --lambda 0.001 --batch-size 1 -lr 1e-4 --save  --exp exp_ywz_balle_En_03_only_q4

#ywz with ccr
python train_vcm.py --config-file /media/data/ccr/VCM/configs/COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x_vcm.yaml

python train_vcm.py --config-file /media/data/yangwenzhe/ywzCompressAI/VCM/configs/COCO-Detection/faster_rcnn_X_101_32x8d_FPN_3x_vcm.yaml

#合体
python3 train_in_this.py


# MAP测试
cd /media/data/minglang/m58786
CUDA_VISIBLE_DEVICES=0
python test.py -i 35 -m evaluation_ml --analysis zeroed_test_ywz --save_path /media/data/yangwenzhe/ywzCompressAI/experiments/exp_test/test/
python test_100.py -i 35 -m evaluation_ml --analysis zeroed_test_ywz --save_path /media/data/yangwenzhe/ywzCompressAI/experiments/exp_test/test_100/

/media/data/liutie/VCM/OpenImageV6-5K
/media/data/liutie/VCM/OpenImageV6-5K_100


# out ori imshow
```
echo $(date +%R)
scp ywz@30901.buaamc2.net:/media/data/yangwenzhe/ywzCompressAI/experiments/exp_test/images/ff659a86a33f815e.png out.png

```

```
import cv2
import numpy as np

import matplotlib.pyplot as plt
# X = [[1, 2], [3, 4], [5, 6]]
# plt.imshow(X)
# plt.colorbar()


f1 = "ori.png"
f2 = "out.png"


png1 = cv2.imread(f1, -1).astype(np.float32)
png2 = cv2.imread(f2, -1).astype(np.float32)
print(png2.shape)
print((png1.max()))
print((png2.max()))
print(png1)
print(png2)

plt.subplot(121)
plt.imshow(png1)
plt.colorbar()
plt.subplot(122)
plt.imshow(png2)
plt.colorbar()
plt.show()


# row, col = 0, 4350
row, col = 0, 0
print("at:[{},{}], png1:{},png2:{}".format(row, col, png1[row, col], png2[row, col]))
```
# 复制前100个文件
ls |head -n 100 |xargs -i cp {} ../pre_100/
# 统计数量
ls -l | grep "^-" | wc -l

# 存训练集
https://www.yuque.com/docs/share/b723c36d-6da2-4d04-a859-bddba3c8bebb?#
```
流程和命令
1.首先准备好数据：从训练集里cp 30k张图，使用 
demo\process_data\rename_ywz.py
里的cp_train_30k_images()
# 将存到/media/data/minglang/m58786/dataset/OpenImage_smaller_30k/

2.修改utils.py文件内
def pick_coco_exp(name, targetlist, split='test'):
的split为train (用于加载训练集的label文件，否则模型跑不起来）
(注意，测试时改回)

3.运行命令
修改test.py里的文件
os.environ["DETECTRON2_DATASETS"] ="/media/data/minglang/m58786/dataset/OpenImage_smaller_30k"
#"/media/data/liutie/VCM/OpenImage_smaller_30k"
为存放30k image的文件夹
(注意，测试时改回：'/media/data/liutie/VCM/OpenImageV6-5K')

4.执行命令
CUDA_VISIBLE_DEVICES=2 python test.py -i 35 -m feature_coding_ml --save_channel_num 0 --save_path "/media/data/minglang/m58786/feature/train_png_30k/"
feature会存放在 --save_path里
```


# 排除复制
cp -r !(file3) /sahil # 只排除当前目录下的一个文件夹  不支持多个
或 安装好sshpass后执行
```Python
import os
all_list = os.listdir()
black_list = ['feature', ]
for name in all_list:
    if black_name in name for black_name in black_list:
        continue
    else:
        # os.system("sudo cp -r {} ../".format(name))
        print(name)
        os.system("sshpass -p r scp -r {} root@172.16.35.61:/data01/minglang/m58786/".format(name))
        break
```

# 修复网段问题
如果使用compose，仅修改daemon.json方法无效。可以采用直接修改yml配置
```
version: '3'
services:
  xx:
    ...
    ...
    #ports:
    ...
    networks:
      - app

networks:
  app:
    driver: bridge
    ipam:
      config:
        - subnet: 10.244.1.0/24
```


# Tensorboard
tensorboard --logdir=/media/data/yangwenzhe/ywzCompressAI/tb_logger/exp_ywz_balle_En_03_only_q4_0.001_irn --port=7001 --bind_all

```
self.belle_writer.add_image('netbelle_output', net_belle_output["x_hat"][0, 0:1, :, :], global_step=self.i_step_count, dataformats='CHW')  # dataformats='HWC')
```